﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoolParser
{
    public class SchoolItem
    {
        public static SchoolItem GetItemFromString(string input, char delim)
        {
            string[] items = input.Split(delim);
            return new SchoolItem()
            {
                Word1 = items[0],
                Word2 = items[1],
                Word3 = items[2],
                SchoolName = items[3]
            };
        }
        public string Word1 {get;set;}
        public string Word2 {get;set;}
        public string Word3 {get;set;}
        public string SchoolName {get;set;}
    }
    public class Parser
    {
        public static IList<SchoolItem>  GetData(string filename, char delim = ';')
        {
           string[] strings = System.IO.File.ReadAllLines(filename);
           IList<SchoolItem> Collection = new List<SchoolItem>();
           for (int i = 0; i < strings.Length; ++i)
           {
              Collection.Add(SchoolItem.GetItemFromString(strings[i], delim)); 
           }
           return Collection;    
        }
    }
}
