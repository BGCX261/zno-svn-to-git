﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.Core
{
    public static class DoubleExtensions
    {
        public static int Round(this double? arg)
        {
            if (!arg.HasValue)
                return 0;

            return (int)Math.Round(arg.Value);
        }
    }
}
