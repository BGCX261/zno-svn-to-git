﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace Zno.Core
{
    public static class CacheManager
    {
        public static T FromCache<T>(string key, Func<T> action)
            where T : class
        {
            var cacheValue = (T)HttpContext.Current.Cache[key];
            if (cacheValue != null)
                return cacheValue;

            var res = action();
            HttpContext.Current.Cache[key] = res;

            return res;
        }
    }
}
