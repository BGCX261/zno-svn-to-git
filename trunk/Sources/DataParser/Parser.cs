﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zno.DomainModels;

namespace DataParser
{
    public class Parser
    {
        public string folderName { get; set; }
        public char delim { get; set; }
        protected DataItem parseString(string value, char separator)
        {
            DataItem Item = new DataItem();
            try
            {
                string[] values = value.Split(separator);
                if (values.Length != 15) throw new Exception("Invalid string");
                
                Item.additionalDistrictId = values[0];
                if (values[1] == "8000000000")
                    values[1] = "80";
                Item.additionalRegionId = Int32.Parse(values[1]);
                Item.SchoolName = values[2];
                Item.passedCount = Int32.Parse(values[13]);
                Item.int1 = Double.Parse(values[3], CultureInfo.InvariantCulture);
                Item.int2 = Double.Parse(values[4], CultureInfo.InvariantCulture);
                Item.int3 = Double.Parse(values[5], CultureInfo.InvariantCulture);
                Item.int4 = Double.Parse(values[6], CultureInfo.InvariantCulture);
                Item.int5 = Double.Parse(values[7], CultureInfo.InvariantCulture);
                Item.int6 = Double.Parse(values[8], CultureInfo.InvariantCulture);
                Item.int7 = Double.Parse(values[9], CultureInfo.InvariantCulture);
                Item.int8 = Double.Parse(values[10], CultureInfo.InvariantCulture);
                Item.int9 = Double.Parse(values[11], CultureInfo.InvariantCulture);
                Item.int10 = Double.Parse(values[12], CultureInfo.InvariantCulture);
                return Item;
            }
            catch
            {
                throw;
            }
        }
        public void Validator(IList<DataItem> items)
        {
            foreach (var item in items)
            {
                if (item.subjectId > 100)
                    Console.WriteLine("Subject id more then 100. ({0})", item.subjectId);
            }
        }
        public IList<DataItem> ParseFolder()
        {
            var startTime = DateTime.Now;
            string[] fileNames = Directory.GetFiles(folderName);
            IList<DataItem> items = new List<DataItem>();
            int totalErrors = 0;
            int year = Int32.Parse(folderName);
            for (int i = 0; i < fileNames.Length; i++)
            {
                string[] lines = File.ReadAllLines(fileNames[i]);
                int id = Int32.Parse(Path.GetFileNameWithoutExtension(fileNames[i]));
                
                for (int j = 0; j < lines.Length; j++)
                {
                    try
                    {
                        var item = parseString(lines[j], delim);
                        item.subjectId = id;
                        item.year = year;
                        items.Add(item);
                    }
                    catch (Exception e)
                    {
                        totalErrors++;
                        Console.WriteLine("Error {4}: {0}. Year: {2}. filename: {3} Check line №{1}", e.Message, j, folderName, id, totalErrors);
                        
                    }

                }
            }
            Console.WriteLine("Used time - {0} seconds. Total errors - {1}", (DateTime.Now-startTime).Seconds, totalErrors);
            return items;
        }
    }
}
//| double summ = Item.int1 + Item.int2 + Item.int3 + Item.int4 + Item.int5 + Item.int6 + Item.int7 + Item.int7 + Item.int8 + Item.int9 + Item.int10;
// if (summ > 102 || summ < 99)
//     throw new Exception(String.Format("Summ less then 100. It's - {0}", summ.ToString()) );
