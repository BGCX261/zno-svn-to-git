﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser parser = new Parser() { folderName = "2010", delim = ';'};
            var items = parser.ParseFolder();
            Console.ReadKey();
        }
    }
}
