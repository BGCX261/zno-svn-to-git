﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zno.Business
{
    public class NHibernateHelper
    {
        private static ISessionFactory sessionFactory;
        private static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    try
                    {
                        var configuration = new NHibernate.Cfg.Configuration();
                        //configuration.DataBaseIntegration(delegate(NHibernate.Cfg.Loquacious.IDbIntegrationConfigurationProperties dbi)
                        //{
                        //    dbi.ConnectionStringName = "Rating_Schools";
                        //    dbi.Dialect<NHibernate.Dialect.MsSql2012Dialect>();
                        //    dbi.Driver<NHibernate.Driver.SqlClientDriver>();
                        //});
                        configuration.Configure();
                        configuration.AddAssembly(typeof(NHibernateHelper).Assembly);
                        sessionFactory = configuration.BuildSessionFactory();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                return sessionFactory;
            }
        }
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        public static T Execute<T>(Func<ISession, T> func)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                try
                {

                    return func(session);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }
        }
    }
}