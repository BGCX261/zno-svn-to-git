﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Zno.DomainModels;

namespace Zno.Business
{
    public class SearchSchoolNameLucene
    {
        public static ICollection<Lucene.Net.Search.ScoreDoc> INTERSECT(ICollection<ScoreDoc> c1, ICollection<ScoreDoc> c2)
        {
            if (c1 == null || c2 == null)
                return null;

            return c1.Intersect(c2).ToList();
        }

        public static List<SearchSchoolModel> SearchSchoolbyName(short RegionID, int DistrictID, string CityName, string SearchString, int CountParameter)
        {
            List<SearchSchoolModel> response = new List<SearchSchoolModel>();
            Directory directory = FSDirectory.Open(new System.IO.DirectoryInfo(ConfigurationSettings.AppSettings["searcLucene"]));
            var analizer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            IndexReader Indexreader = IndexReader.Open(directory, true);
            Searcher IndexSearch = new IndexSearcher(Indexreader);

            var QueryParserName = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, "Name", analizer);

            var QueryParserCity = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, "City", analizer);
            QueryParserName.AllowLeadingWildcard = false;

            Query RegionFilter = null;
            Query DistrictFilter = null;
            Query CityFilter = null;
            SearchString = SearchString.Replace("\"", "'");
            List<string> searchstr = SearchString.Split(' ', '.', ',', ';', ':', '/', '\\', '?', '!', '-', '\'', '№', '’', ')', '(', '{', '}', '[', ']', '*', '~', '!', '@', '#', '$', '%', '%', '^', '&', '*', '+', '=', '₴', '`', '>', '<', '|').ToList();
            foreach (string s in searchstr.ToArray())
            {
                if (s.Length == 0 || s.All(arg => Char.IsPunctuation(arg)))
                {
                    searchstr.Remove(s);
                }
            }
            //searchStrFor = searchstr;
            string QueryString = "";
            for (int i = 0; i < searchstr.Count; i++)
            {
                if (searchstr[i].Length >= 1)
                {
                    QueryString += "*" + searchstr[i] + "* AND ";
                }
            }
            QueryString = QueryString.Substring(0, QueryString.Length - 5);
            Query SearchSchoolFilter = null;
            try
            {
                QueryParserName.AllowLeadingWildcard = true;
                SearchSchoolFilter = QueryParserName.Parse(QueryString);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            BooleanQuery ResultQuery = new BooleanQuery();
            if (RegionID == 0)
            {
                //just SearchString
                ResultQuery.Add(SearchSchoolFilter, Occur.MUST);
            }
            else if (DistrictID == 0 && CityName == "")
            {
                RegionFilter = new TermQuery(new Term("RegionId", RegionID.ToString()));
                ResultQuery.Add(RegionFilter, Occur.MUST);
                ResultQuery.Add(SearchSchoolFilter, Occur.MUST);
            }
            else if (DistrictID == 0)
            {
                //just SearchString, RegionID, CityName
                RegionFilter = new TermQuery(new Term("RegionId", RegionID.ToString()));
                CityFilter = QueryParserCity.Parse(CityName);

                ResultQuery.Add(RegionFilter, Occur.MUST);
                ResultQuery.Add(CityFilter, Occur.MUST);
                ResultQuery.Add(SearchSchoolFilter, Occur.MUST);
            }
            else
            {
                //just SearchString, RegionID, DistrictID
                RegionFilter = new TermQuery(new Term("RegionId", RegionID.ToString()));
                DistrictFilter = new TermQuery(new Term("DistrictId", DistrictID.ToString()));

                ResultQuery.Add(RegionFilter, Occur.MUST);
                ResultQuery.Add(DistrictFilter, Occur.MUST);
                ResultQuery.Add(SearchSchoolFilter, Occur.MUST);
            }


            TopDocs resultDocs = IndexSearch.Search(ResultQuery, Indexreader.MaxDoc);
            if (CountParameter != 1)
            {
                var hits = resultDocs.ScoreDocs.OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList();
                if (hits.Count == 0)
                {
                    //бить слова попалам
                }
                ICollection<Lucene.Net.Search.ScoreDoc> resDigitsFull = new List<Lucene.Net.Search.ScoreDoc>();
                ICollection<Lucene.Net.Search.ScoreDoc> resStartWirhFirst = new List<Lucene.Net.Search.ScoreDoc>();
                ICollection<Lucene.Net.Search.ScoreDoc> resIncludeFirstFull = new List<Lucene.Net.Search.ScoreDoc>();
                int j = 0;
                foreach (var hit in hits)
                {
                    string str = IndexSearch.Doc(hit.Doc).Get("Name").ToLower();
                    Regex exp = new Regex("[0-9]+");
                    MatchCollection coll = exp.Matches(QueryString);
                    if (coll.Count != 0)
                    {
                        string rexex = "";
                        foreach (Match par in coll)
                        {
                            rexex += "([\\D]+|^)" + par.ToString();
                        }
                        rexex += "([\\D]+|$)";
                        exp = new Regex(rexex);
                        coll = exp.Matches(str);
                        if (coll.Count != 0)
                        {
                            resDigitsFull.Add(hit);
                        }
                    }
                    string first = searchstr[0].ToLower();
                    exp = new Regex("(\\W|^)" + first + "(\\W|$)");
                    coll = exp.Matches(str);
                    if (coll.Count != 0)
                    {
                        resIncludeFirstFull.Add(hit);
                    }
                    exp = new Regex("^" + first);
                    coll = exp.Matches(str);
                    if (coll.Count != 0)
                    {
                        resStartWirhFirst.Add(hit);
                    }
                }
                IEnumerable<Lucene.Net.Search.ScoreDoc> resp = new List<Lucene.Net.Search.ScoreDoc>();

                ICollection<ScoreDoc> result = new List<ScoreDoc>();
                if (resDigitsFull.Count() == 0 && resIncludeFirstFull.Count() == 0 && resStartWirhFirst.Count() == 0)
                {
                    hits = hits.OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).Take(10).ToList();
                }
                else
                {
                    resp = INTERSECT(resStartWirhFirst, INTERSECT(resDigitsFull, resIncludeFirstFull));
                    if (resp.Count() >= 10)
                    {
                        result = resp.OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).Take(10).ToList();
                    }
                    else
                    {
                        result = resp.ToList();
                        foreach (var hit in resp.ToList())
                        {
                            resDigitsFull.Remove(hit);
                            resIncludeFirstFull.Remove(hit);
                            resStartWirhFirst.Remove(hit);
                        }

                        List<ScoreDoc> res1 = (INTERSECT(resDigitsFull, resIncludeFirstFull).OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList());

                        foreach (var r in res1)
                        {
                            if (result.Count() >= 10) break;
                            result.Add(r);
                        }
                        if (result.Count < 10)
                        {
                            foreach (var hit in result)
                            {
                                resDigitsFull.Remove(hit);
                                resIncludeFirstFull.Remove(hit);
                                resStartWirhFirst.Remove(hit);
                            }
                            List<ScoreDoc> res2 = (INTERSECT(resDigitsFull, resStartWirhFirst).OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList());
                            foreach (var r in res2)
                            {
                                if (result.Count >= 10) break;
                                result.Add(r);
                            }
                            if (result.Count < 10)
                            {
                                foreach (var hit in result)
                                {
                                    resDigitsFull.Remove(hit);
                                    resIncludeFirstFull.Remove(hit);
                                    resStartWirhFirst.Remove(hit);
                                }
                                List<ScoreDoc> res3 = (INTERSECT(resIncludeFirstFull, resStartWirhFirst).OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList());
                                foreach (var r in res3)
                                {
                                    if (result.Count >= 10) break;
                                    result.Add(r);
                                }
                                if (result.Count < 10)
                                {
                                    foreach (var hit in result)
                                    {
                                        resDigitsFull.Remove(hit);
                                        resIncludeFirstFull.Remove(hit);
                                        resStartWirhFirst.Remove(hit);
                                    }
                                    foreach (var r in resDigitsFull)
                                    {
                                        if (result.Count >= 10) break;
                                        result.Add(r);
                                    }
                                    if (result.Count < 10)
                                    {
                                        foreach (var hit in result)
                                        {
                                            resDigitsFull.Remove(hit);
                                            resIncludeFirstFull.Remove(hit);
                                            resStartWirhFirst.Remove(hit);
                                        }
                                        foreach (var r in resIncludeFirstFull)
                                        {
                                            if (result.Count >= 10) break;
                                            result.Add(r);
                                        }
                                        if (result.Count < 10)
                                        {
                                            foreach (var hit in result)
                                            {
                                                resDigitsFull.Remove(hit);
                                                resIncludeFirstFull.Remove(hit);
                                                resStartWirhFirst.Remove(hit);
                                            }
                                            foreach (var r in resStartWirhFirst)
                                            {
                                                if (result.Count >= 10) break;
                                                result.Add(r);
                                            }
                                            if (result.Count < 10)
                                            {
                                                foreach (var hit in result)
                                                {
                                                    hits.Remove(hit);
                                                }
                                                foreach (var hit in hits)
                                                {
                                                    if (result.Count >= 10) break;
                                                    result.Add(hit);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    hits = result.ToList();
                }
                foreach (var hit in hits)
                {
                    var DocumentFromSearch = IndexSearch.Doc(hit.Doc);
                    response.Add(new SearchSchoolModel { Id = Int32.Parse(DocumentFromSearch.Get("Id")), Name = DocumentFromSearch.Get("Name") });
                }
            }

            else
            {
                /*var hits = resultDocs.ScoreDocs.OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name"));
                foreach (var hit in hits)
                {
                    var DocumentFromSearch = IndexSearch.Doc(hit.Doc);
                    response.Add(new SearchSchoolModel { Id = Int32.Parse(DocumentFromSearch.Get("Id")), Name = DocumentFromSearch.Get("Name") });
                }*/
                var hits = resultDocs.ScoreDocs.OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList();
                if (hits.Count == 0)
                {
                    //бить слова попалам
                }
                ICollection<Lucene.Net.Search.ScoreDoc> resDigitsFull = new List<Lucene.Net.Search.ScoreDoc>();
                ICollection<Lucene.Net.Search.ScoreDoc> resStartWirhFirst = new List<Lucene.Net.Search.ScoreDoc>();
                ICollection<Lucene.Net.Search.ScoreDoc> resIncludeFirstFull = new List<Lucene.Net.Search.ScoreDoc>();

                foreach (var hit in hits)
                {
                    string str = IndexSearch.Doc(hit.Doc).Get("Name").ToLower();
                    Regex exp = new Regex("[0-9]+");
                    MatchCollection coll = exp.Matches(QueryString);
                    if (coll.Count != 0)
                    {
                        string rexex = "";
                        foreach (Match par in coll)
                        {
                            rexex += "([\\D]+|^)" + par.ToString();
                        }
                        rexex += "([\\D]+|$)";
                        exp = new Regex(rexex);
                        coll = exp.Matches(str);
                        if (coll.Count != 0)
                        {
                            resDigitsFull.Add(hit);
                        }
                    }
                    string first = searchstr[0].ToLower();
                    exp = new Regex("(\\W|^)" + first + "(\\W|$)");
                    coll = exp.Matches(str);
                    if (coll.Count != 0)
                    {
                        resIncludeFirstFull.Add(hit);
                    }
                    exp = new Regex("^" + first);
                    coll = exp.Matches(str);
                    if (coll.Count != 0)
                    {
                        resStartWirhFirst.Add(hit);
                    }
                }
                IEnumerable<Lucene.Net.Search.ScoreDoc> resp = new List<Lucene.Net.Search.ScoreDoc>();
                ICollection<ScoreDoc> result = new List<ScoreDoc>();
                if (!(resDigitsFull.Count() == 0 && resIncludeFirstFull.Count() == 0 && resStartWirhFirst.Count() == 0))
                {
                    resp = INTERSECT(resStartWirhFirst, INTERSECT(resDigitsFull, resIncludeFirstFull));
                    result = resp.OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList();

                    foreach (var hit in resp.ToList())
                    {
                        resDigitsFull.Remove(hit);
                        resIncludeFirstFull.Remove(hit);
                        resStartWirhFirst.Remove(hit);
                    }

                    List<ScoreDoc> res1 = (INTERSECT(resDigitsFull, resIncludeFirstFull).OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList());

                    foreach (var r in res1)
                    {
                        result.Add(r);
                    }


                    foreach (var hit in result)
                    {
                        resDigitsFull.Remove(hit);
                        resIncludeFirstFull.Remove(hit);
                        resStartWirhFirst.Remove(hit);
                    }
                    List<ScoreDoc> res2 = (INTERSECT(resDigitsFull, resStartWirhFirst).OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList());
                    foreach (var r in res2)
                    {
                        result.Add(r);
                    }
                    foreach (var hit in result)
                    {
                        resDigitsFull.Remove(hit);
                        resIncludeFirstFull.Remove(hit);
                        resStartWirhFirst.Remove(hit);
                    }
                    List<ScoreDoc> res3 = (INTERSECT(resIncludeFirstFull, resStartWirhFirst).OrderBy(arg => IndexSearch.Doc(arg.Doc).Get("Name").Length).ToList());
                    foreach (var r in res3)
                    {
                        result.Add(r);
                    }
                    foreach (var hit in result)
                    {
                        resDigitsFull.Remove(hit);
                        resIncludeFirstFull.Remove(hit);
                        resStartWirhFirst.Remove(hit);
                    }
                    foreach (var r in resDigitsFull)
                    {
                        result.Add(r);
                    }

                    foreach (var hit in result)
                    {
                        resDigitsFull.Remove(hit);
                        resIncludeFirstFull.Remove(hit);
                        resStartWirhFirst.Remove(hit);
                    }
                    foreach (var r in resIncludeFirstFull)
                    {
                        result.Add(r);
                    }
                    foreach (var hit in result)
                    {
                        resDigitsFull.Remove(hit);
                        resIncludeFirstFull.Remove(hit);
                        resStartWirhFirst.Remove(hit);
                    }
                    foreach (var r in resStartWirhFirst)
                    {
                        result.Add(r);
                    }
                    foreach (var hit in result)
                    {
                        hits.Remove(hit);
                    }
                    foreach (var hit in hits)
                    {
                        result.Add(hit);
                    }
                    hits = result.ToList();
                }


                foreach (var hit in hits)
                {
                    var DocumentFromSearch = IndexSearch.Doc(hit.Doc);
                    response.Add(new SearchSchoolModel { Id = Int32.Parse(DocumentFromSearch.Get("Id")), Name = DocumentFromSearch.Get("Name") });
                }
            }
            return response;
        }
    }
}
