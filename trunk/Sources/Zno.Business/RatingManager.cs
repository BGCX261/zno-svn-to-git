﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zno.Core;
using Zno.DomainModels;

namespace Zno.Business
{
    public static class RatingManager
    {
        public static IList<DistrictModel> GetDistrictList(short regionId)
        {
            var cacheKey = "GetDistrictList_" + regionId;

            return CacheManager.FromCache(cacheKey, () => RatingMethod.GetDistrictList(regionId));
        }
    }
}
