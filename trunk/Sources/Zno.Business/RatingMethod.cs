﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zno.Core;
using Zno.DomainModels;

namespace Zno.Business
{
    public class RatingMethod
    {
        public static IList<SchoolModel> GetSchoolsByDistrict(short regionId, int districtId, string cityName, int pageindex = 0)
        {
            return NHibernateHelper.Execute(session =>
            {
                int pagesize = 16;
                var query = session.Query<School>();

                if (regionId != 0)
                {
                    query = query.Where(qi => qi.District.Region.RegionID == regionId);

                    if (districtId == 0)
                    {
                        if (!string.IsNullOrEmpty(cityName))
                            query = query.Where(qi => qi.City == cityName);
                    }
                    else
                    {
                        query = query.Where(qi => qi.District.DistrictID == districtId);
                    }
                }

                query = query.OrderByDescending(arg => arg.GeneralRating).Skip(pageindex * pagesize).Take(pagesize);

                return query.Select(s => new SchoolModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    District = s.District.DistrictName,
                    Description = s.Description,
                    Region = s.District.Region.RegionName,
                    GeneralRating = s.GeneralRating,
                    ExactSciences = s.ExactSciences,
                    HumanitiesSciences = s.HumanitiesSciences,
                    NaturalSciences = s.NaturalSciences,
                    Address = s.Address,
                    NumberUkr = s.RatingUkr,
                    NumberRegion = s.RatingRegion,
                    NumberDistrict = s.RatingDistrict
                }).ToList();
            });

        }

        public static Zno.DomainModels.BagSchoolsModel GetBagRecords(ICollection<int> schoolsId)
        {
            return NHibernateHelper.Execute(session =>
            {
                var SchoolsInfo = (from s in session.Query<School>() where schoolsId.Contains(s.Id) select s)
                    .ToList().OrderBy(arg => GetPos(schoolsId.ToArray<int>(), arg.Id)).ToList();
                var SchoolsSubject = (from s in session.Query<SchoolInfo>()
                                      where (schoolsId.Contains(s.SchoolId) && s.Year == 0)
                                      select s)
                                      .ToList().OrderBy(arg => GetPos(schoolsId.ToArray<int>(), arg.SchoolId)).ToList();
                //first parameter
                var Info = new BagSchoolsInfo[schoolsId.Count];
                var GenRating = new SchoolRating[4];
                var SubjRating = new SchoolRating[16];
                var NamesGenRating = new string[] { "Загальний рейтинг", "Гуманітарні науки", "Точні науки", "Природничі науки" };
                var NamesSubject = new string[]{ "Математика", "Фізика", "Українська мова та література", "Історія України", "Всесвітня історія",
                    "Зарубіжна література", "Англійська мова", "Німецька мова", "Французька мова", "Іспанська мова",
                    "Російська мова", "Економіка", "Правознавство", "Біологія", "Географія", "Хімія"};
                int i = 0;

                var Points = new int[4][];
                for (i = 0; i < Points.Length; ++i)
                    Points[i] = new int[schoolsId.Count];

                var PointsSubj = new int[16][];
                for (i = 0; i < PointsSubj.Length; ++i)
                    PointsSubj[i] = new int[schoolsId.Count];

                i = 0;
                foreach (var info in SchoolsInfo)
                {
                    Points[0][i] = info.GeneralRating.Round();
                    Points[1][i] = info.HumanitiesSciences.Round();
                    Points[2][i] = info.ExactSciences.Round();
                    Points[3][i] = info.NaturalSciences.Round();
                    i++;
                }
                i = 0;
                foreach (var subj in SchoolsSubject)
                {
                    PointsSubj[0][i] = subj.Math.Round();
                    PointsSubj[1][i] = subj.Phisics.Round();
                    PointsSubj[2][i] = subj.Ukrainian.Round();
                    PointsSubj[3][i] = subj.HistoryOfUkr.Round();
                    PointsSubj[4][i] = subj.WorldHistory.Round();
                    PointsSubj[5][i] = subj.ForeignLiteraturue.Round();
                    PointsSubj[6][i] = subj.English.Round();
                    PointsSubj[7][i] = subj.Germany.Round();
                    PointsSubj[8][i] = subj.Franch.Round();
                    PointsSubj[9][i] = subj.Spanish.Round();
                    PointsSubj[10][i] = subj.Russian.Round();
                    PointsSubj[11][i] = subj.Economics.Round();
                    PointsSubj[12][i] = subj.Jurisprudence.Round();
                    PointsSubj[13][i] = subj.Biology.Round();
                    PointsSubj[14][i] = subj.Geography.Round();
                    PointsSubj[15][i] = subj.Chemistry.Round();
                    i++;
                }
                i = 0;
                foreach (var info in SchoolsInfo)
                {
                    Info[i] = new BagSchoolsInfo { Id = info.Id, Name = info.Name.Replace('"', '&'), Address = info.Address.Replace('"', '&'), Description = info.Description.Replace('"', '&') };
                    i++;
                }
                for (i = 0; i < 4; i++)
                {
                    GenRating[i] = new SchoolRating { RatingName = NamesGenRating[i], Points = Points[i] };
                }
                for (i = 0; i < 16; i++)
                {
                    SubjRating[i] = new SchoolRating { RatingName = NamesSubject[i], Points = PointsSubj[i] };
                }
                var response = new BagSchoolsModel { SchoolsInfo = Info, GeneralRating = GenRating, SubjectRating = SubjRating };
                return response;
            });
        }

        public static SchoolData GetSchoolInfoById(int id)
        {
            return NHibernateHelper.Execute(session =>
            {
                var response = session.Query<SchoolInfo>().Where(arg => arg.SchoolId == id).ToList();
                var school = session.QueryOver<SchoolShortData>().Where(data => data.Id == id).SingleOrDefault();
                var znoPassedCount = session.QueryOver<SchoolZnoInfo>().Where(data => data.Id == id).List();

                return new SchoolData { Info = response, SchoolInfo = school, ZnoInfo = znoPassedCount };
            });
        }
        public static IList<SchoolModel> Get(int pageIndex = 0, int pageSize = 16)
        {
            return NHibernateHelper.Execute(session =>
            {
                var q = (from School b in session.Query<School>()
                         select new SchoolModel()
                         {
                             Id = b.Id,
                             Name = b.Name,
                             District = b.District.DistrictName,
                             Description = b.Description,
                             Region = b.District.Region.RegionName,
                             GeneralRating = b.GeneralRating,
                             ExactSciences = b.ExactSciences,
                             HumanitiesSciences = b.HumanitiesSciences,
                             NaturalSciences = b.NaturalSciences,
                             Address = b.Address
                         }).OrderByDescending(arg => arg.GeneralRating)
                    .Skip(pageSize * pageIndex).Take(pageSize).ToList<SchoolModel>();

                return q;
            });
        }

        public static IList<DistrictModel> GetDistrictList(short regionId)
        {
            return NHibernateHelper.Execute(session =>
            {
                var resp =  (from d in session.Query<District>()
                        where d.Region.RegionID == regionId
                        orderby d.DistrictName
                        select new DistrictModel { DistrictName = d.DistrictName, Id = d.DistrictID }
                    ).ToList();
                return resp;
            });
        }

        public static IList<CityModel> GetCityList(short regionId)
        {
            return NHibernateHelper.Execute(session =>
            {
                var resp =  (session.Query<School>()
                    .Where(arg => arg.District.Region.RegionID == regionId && arg.City != null && arg.City.Length > 0)
                    .Select(arg => arg.City)).Distinct().ToList()
                    .Select(v => new CityModel { CityName = v }).ToList();
                return resp;
            });
        }

        public static IList<SchoolModel> GetByCity(string cityName)
        {
            return NHibernateHelper.Execute(session =>
            {
                var response = (from School b in session.Query<School>()
                                where b.City.Contains(cityName) || cityName.Contains(b.City)
                                select new SchoolModel()
                                {
                                    Id = b.Id,
                                    Name = b.Name,
                                    District = b.District.DistrictName,
                                    Description = b.Description,
                                    Region = b.District.Region.RegionName,
                                    GeneralRating = b.GeneralRating,
                                    ExactSciences = b.ExactSciences,
                                    HumanitiesSciences = b.HumanitiesSciences,
                                    NaturalSciences = b.NaturalSciences,
                                    Address = b.Address
                                }).OrderByDescending(arg => arg.GeneralRating).ToList<SchoolModel>();
                return response;
            });
        }

        public static int GetPos(int[] Sch, int id)
        {
            int index = Array.IndexOf(Sch, id);
            return index == -1 ? Sch.Length : index;
        }

        public static IList<SchoolModel> GetSchoolsByID(ICollection<int> SchoolIdList, int pageindex)
        {
            return NHibernateHelper.Execute(session =>
            {
                var response = (from arg in session.Query<School>()
                                where SchoolIdList.Contains(arg.Id)
                                select new SchoolModel()
                                                   {
                                                       Name = arg.Name,
                                                       District = arg.District.DistrictName,
                                                       Description = arg.Description,
                                                       Region = arg.District.Region.RegionName,
                                                       GeneralRating = arg.GeneralRating,
                                                       ExactSciences = arg.ExactSciences,
                                                       HumanitiesSciences = arg.HumanitiesSciences,
                                                       NaturalSciences = arg.NaturalSciences,
                                                       Address = arg.Address,
                                                       Id = arg.Id,
                                                       NumberUkr = arg.RatingUkr,
                                                       NumberRegion = arg.RatingRegion,
                                                       NumberDistrict = arg.RatingDistrict
                                                   }).ToList<SchoolModel>().OrderBy(arg => GetPos(SchoolIdList.ToArray<int>(), arg.Id)).ToList();
                return response;
            });
        }
        
        #region Methods to get all records of table

        public static IList<ZnoModel> GetAllZnoRecords()
        {
            return NHibernateHelper.Execute(session =>
            {
                var list = session.QueryOver<ZnoModel>().List();
                return list;
            });
        }
        #endregion
    }
}
