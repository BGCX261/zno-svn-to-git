﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            string D = "\"(\\d+)\",";
            string LD = "\"(\\d+)\"";
            FileRegInfo[] files = new FileRegInfo[5];
            files[0] = new FileRegInfo("school_addresses.sql", "([0-9]*), '([^']*)', ([0-9]*), '?([^']*)'?, '([^']*)', '([^']*)', ([NULL0-9.]+), ([NULL0-9.]+), '([^']*)'", true);
            files[1] = new FileRegInfo("iwe_stat_entity.sql", "([0-9NULL]*), ([0-9NULL]*), '([^']+)', ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*)", true);
            files[2] = new FileRegInfo("subjects.txt", "\"([\\dа-яА-ЯіІЇїЄєҐґ ]+)\"");
            files[3] = new FileRegInfo("region.txt", "\"([\\d\\wа-яА-Я'іІЇїЄєҐґ\\-, ]+)\"");
            files[4] = new FileRegInfo("olbast.txt", "\"([\\dа-яА-ЯіІЇїЄєҐґ'\\-. ]+)\"");
            DateTime start = DateTime.Now;
            item[] it = null;

                it = FileParser.GetCollection(files[1]);
                for (int i = 0; i < it.Length; i++ )
                {
                    if (it[i].field.Length != 18)
                        Console.Write("Not match in line {0}", i+1);
                }
                    Console.WriteLine("Parsed {0} lines for {1}sec.", it.Length, (DateTime.Now - start).Seconds);
     
            Console.Read();
        }
    }
}
