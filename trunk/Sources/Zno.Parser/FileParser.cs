﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
namespace Zno.Parser
{
    class FileRegInfo
    {
        public string filename { get; set; }
        public string pattern { get; set; }
        public readonly bool asOne;
        public FileRegInfo(string FName, string RegPatternt, bool asOne = false)
        {
            filename = FName;
            pattern = RegPatternt;
            this.asOne = asOne;
        }
    }
    class item
    {
        public object[] field;
        public item(int size)
        {
            field = new object[size];
        }
    }

    class FileParser
    {
        static protected StreamReader SR;
        static protected int getLines(FileRegInfo file)
        {
            return File.ReadAllLines(file.filename).Length;
        }
        static protected item[] collectionA(FileRegInfo file)
        {
            Open(file);
            int count = getLines(file);
            int k = 0;
            item[] Collection = new item[count];
            Regex reg = new Regex(file.pattern);
            for (int i = 0; i < count; ++i )
            {
                string str = SR.ReadLine();
                if (str.Length < 5)
                    continue;
                k++;
                Match match = reg.Match(str);
                Collection[i] = new item(match.Groups.Count);
                int res = 0;
                for (int j = 0; j < match.Groups.Count; ++j, res = -1)
                {
                    try
                    {
                        if (int.TryParse(match.Groups[j].Value, out res))
                            Collection[i].field[j] = res;
                        else
                            Collection[i].field[j] = match.Groups[j].Value;
                    }
                    catch
                    {
                        Console.WriteLine("Problem in {0} line", i + 1);
                    }
                }
            }
            Close();
            Console.WriteLine("In File {0} {1} records", file.filename, k);
                return Collection;
        }
        static protected item[] collectionB(FileRegInfo file)
        {
            Open(file);
            int count = getLines(file);
            Console.WriteLine("In File {0} {1} records", file.filename, count);
            item[] Collection = new item[count];
            Regex reg = new Regex(file.pattern);
            int res;
            for (int i = 0; i < count; ++i)
            {
                MatchCollection matches = reg.Matches(SR.ReadLine());

                Collection[i] = new item(matches.Count);

                for (int j = 0; j < matches.Count; ++j)
                {
                    try
                    {
                        if (int.TryParse(matches[j].Groups[0].Value, out res))
                            Collection[i].field[j] = res;
                        else
                            Collection[i].field[j] = matches[j].Groups[1].Value;
                    }
                    catch
                    {
                        Console.WriteLine("Problem in {0} line", i + 1);
                    }
                }
            }
            Close();
            return Collection;
        }
        static public item[] GetCollection(FileRegInfo file)
        { 
            if (file.asOne)
                return collectionA(file);

            return collectionB(file);
        }
        static protected void Open(FileRegInfo file)
        {
            SR = new StreamReader(file.filename);
        }
        static protected void Close()
        {
            SR.Close();
        }
        
    }
}
