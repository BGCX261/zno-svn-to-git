﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class SchoolZnoInfo
    {
        public virtual int ZnoID { get; set; }
        public virtual int Id { get; set; }
        public virtual string SubjectName { get; set; }
        public virtual int Year { get; set; }
        public virtual int Count { get; set; }
        public virtual int SubjectId { get; set; } 
    }

}
