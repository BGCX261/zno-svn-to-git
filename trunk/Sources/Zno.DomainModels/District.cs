﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zno.DomainModels
{
    public class District
    {
        public virtual int DistrictID { get; set; }
        public virtual Region Region { get; set; }
        public virtual string DistrictName { get; set; }
        public virtual string DistrictAddID { get; set; }
        public virtual ICollection<School> School { get; set; }
        public virtual ICollection<ZnoModel> Zno { get; set; }
    }
}