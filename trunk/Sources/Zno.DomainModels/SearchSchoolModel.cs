﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class SearchSchoolModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
