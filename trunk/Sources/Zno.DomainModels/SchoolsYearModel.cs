﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class SchoolsYearModel
    {
        public string Year { get; set; }
        public double?[] GenRating { get; set; }
        public double?[] ExactRating { get; set; }
        public double?[] HumanRating { get; set; }
        public double?[] NaturalRating { get; set; }
        public double?[] HistoryOfUkr { get; set; }
        public double?[] Biology { get; set; }
        public double?[] WorldHistory { get; set; }
        public double?[] Geography { get; set; }
        public double?[] ForeignLiteraturue { get; set; }
        public double?[] Math { get; set; }
        public double?[] Economics { get; set; }
        public double?[] Jurisprudence { get; set; }
        public double?[] Ukrainian { get; set; }
        public double?[] Phisics { get; set; }
        public double?[] Chemistry { get; set; }
        public double?[] Spanish { get; set; }
        public double?[] English { get; set; }
        public double?[] Germany { get; set; }
        public double?[] Franch { get; set; }
        public double?[] Russian { get; set; }
    }
}
