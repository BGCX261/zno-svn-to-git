﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zno.DomainModels
{
    public class Subject
    {
        //public Subject() 
        //{
        //    this.Zno = new HashSet<Zno>(); 
        //}
        public virtual short SubjectID { get; set; }
        public virtual string SubjectName { get; set; }
        public virtual short Speciality { get; set; }
        public virtual ICollection<ZnoModel> Zno { get; set; }
    }
}