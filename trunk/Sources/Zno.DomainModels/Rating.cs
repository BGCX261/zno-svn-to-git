﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zno.DomainModels
{
    public class Rating
    {
        public virtual int RatingID { get; set; }
        public virtual ZnoModel Zno { get; set; }
        public virtual double Point { get; set; }
    }
}