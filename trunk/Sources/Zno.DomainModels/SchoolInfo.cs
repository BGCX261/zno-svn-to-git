﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{

    public class SchoolInfo
    {
        public virtual int Id { get; set; }
        public virtual double? GenRating { get; set; }
        public virtual double? HumRating { get; set; }
        public virtual double? ExactRating { get; set; }
        public virtual double? NatRating { get; set; }
        public virtual int Year { get; set; }
        public virtual int SchoolId { get; set; }
        public virtual double? HistoryOfUkr { get; set; }
        public virtual double? Biology { get; set; }
        public virtual double? WorldHistory { get; set; }
        public virtual double? Geography { get; set; }
        public virtual double? ForeignLiteraturue { get; set; }
        public virtual double? Math { get; set; }
        public virtual double? Economics { get; set; }
        public virtual double? Jurisprudence { get; set; }
        public virtual double? Ukrainian { get; set; }
        public virtual double? Phisics { get; set; }
        public virtual double? Chemistry { get; set; }
        public virtual double? Spanish { get; set; }
        public virtual double? English { get; set; }
        public virtual double? Germany { get; set; }
        public virtual double? Franch { get; set; }
        public virtual double? Russian { get; set; }
    }
}
