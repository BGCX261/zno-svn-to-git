﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zno.DomainModels
{
    
    public class SchoolModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public Nullable<double> GeneralRating { get; set; }
        public Nullable<double> ExactSciences { get; set; }
        public Nullable<double> HumanitiesSciences { get; set; }
        public Nullable<double> NaturalSciences { get; set; }
        public string City { get; set; }
        public Nullable<double> Langitude { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<int> NumberUkr { get; set; }
        public Nullable<int> NumberRegion { get; set; }
        public Nullable<int> NumberDistrict { get; set; }
    }
    
}