﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zno.DomainModels
{
    public class Region
    {
        //public Region() { this.District = new HashSet<District>(); }
        public virtual short RegionID { get; set; }
        public virtual string RegionName { get; set; }
        public virtual int RegionAddID { get; set; }
        public virtual ICollection<District> District { get; set; }
    }
}