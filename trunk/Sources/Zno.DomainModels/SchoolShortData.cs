﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class SchoolShortData
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual string Description { get; set; }
        public virtual string DistrictName { get; set; }
        public virtual string RegionName { get; set; }
        public virtual double? Longitude { get; set; }
        public virtual double? Latitude { get; set; }
        public virtual double? GeneralRating { get; set; }
    }
}
