﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class BagSchoolsInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
    }
    public class SchoolRating
    {
        public string RatingName { get; set; }
        public int[] Points { get; set; }
    };
    public class BagSchoolsModel
    {
        public BagSchoolsInfo[] SchoolsInfo { get; set; }
        public SchoolRating[] GeneralRating { get; set; }
        public SchoolRating[] SubjectRating { get; set; }
    }
}
