﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class SchoolData
    {
        public IList<SchoolInfo> Info { get; set; }
        public SchoolShortData SchoolInfo { get; set; }
        public IList<SchoolZnoInfo> ZnoInfo { get; set; }
    }

    public class School
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string City { get; set; }
        public virtual string Address { get; set; }
        public virtual string Description { get; set; }
        public virtual Nullable<double> Longitude { get; set; }
        public virtual Nullable<double> Latitude { get; set; }
        public virtual string SearchString { get; set; }
        public virtual District District { get; set; }
        public virtual Nullable<double> GeneralRating { get; set; }
        public virtual Nullable<double> ExactSciences { get; set; }
        public virtual Nullable<double> HumanitiesSciences { get; set; }
        public virtual Nullable<double> NaturalSciences { get; set; }
        public virtual Nullable<int> RatingUkr { get; set; }
        public virtual Nullable<int> RatingRegion { get; set; }
        public virtual Nullable<int> RatingDistrict { get; set; }
    }
}
