﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class DataItem
    {
        public virtual int id { get; set; }
        public virtual string additionalDistrictId { get; set; }
        public virtual int additionalRegionId { get; set; }
        public virtual int districtId { get; set; }
        public virtual int regionId { get; set; }
        public virtual string SchoolName { get; set; }

        public virtual int passedCount { get; set; }
        public virtual int subjectId { get; set; }
        public virtual int year { get; set; }
        public virtual double int1 { get; set; }
        public virtual double int2 { get; set; }
        public virtual double int3 { get; set; }
        public virtual double int4 { get; set; }
        public virtual double int5 { get; set; }
        public virtual double int6 { get; set; }
        public virtual double int7 { get; set; }
        public virtual double int8 { get; set; }
        public virtual double int9 { get; set; }
        public virtual double int10 { get; set; }
    }
}
