﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class DistrictModel
    {
        public string DistrictName { get; set; }
        public int Id { get; set; }
    }

    public class CityModel
    {
        public string CityName { get; set; }
    }
}
