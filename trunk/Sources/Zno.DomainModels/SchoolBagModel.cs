﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class SchoolItem
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Address{ get; set; }
        public string Description{ get; set; }
        public string Color{ get; set; }
    }
    public class SchoolBagModel
    {
        public SchoolItem[] School;
        public SchoolsYearModel[] Year;
    }
}
