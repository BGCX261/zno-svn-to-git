﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zno.DomainModels
{
    public class ZnoKapec
    {
        
            public virtual int ZapId { get; set; }
            public virtual int ZnoID { get; set; }
            public virtual District District { get; set; }
            public virtual Subject Subject { get; set; }
            public virtual short Year { get; set; }
            public virtual string ZnoSchoolName { get; set; }
            public virtual int PassedCount { get; set; }
            public virtual Nullable<int> Interval1 { get; set; }
            public virtual Nullable<int> Interval2 { get; set; }
            public virtual Nullable<int> Interval3 { get; set; }
            public virtual Nullable<int> Interval4 { get; set; }
            public virtual Nullable<int> Interval5 { get; set; }
            public virtual Nullable<int> Interval6 { get; set; }
            public virtual Nullable<int> Interval7 { get; set; }
            public virtual Nullable<int> Interval8 { get; set; }
            public virtual Nullable<int> Interval9 { get; set; }
            public virtual Nullable<int> Interval10 { get; set; }
            public virtual double? Rating { get; set; }
        
    }
}
