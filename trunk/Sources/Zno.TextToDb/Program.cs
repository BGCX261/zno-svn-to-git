﻿using DataParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zno.Business;
using Zno.DomainModels;
namespace Zno.TextToDb
{
    class DisctrictReplace
    {
        public string s1 { get; set; }
        public string s2 { get; set; }
    }
    class Program
    {
        protected static IList<DisctrictReplace> bindingDistrcitId { get; set; }
        protected static void LoadDefaultReplacer()
        {
            bindingDistrcitId = new List<DisctrictReplace>();
            //bindingDistrcitId.Add(new DisctrictReplace() // for 2010 year hot fix
            //{
            //    s1 = "14230000",
            //    s2 = "14133000"
            //});
        }
        public static void LoadToDb(string folderName)
        {
            Console.WriteLine("Parsing folder - {0}",folderName);
            Parser parser = new Parser() { folderName = folderName, delim = ';' };
            var items = parser.ParseFolder();
            Console.WriteLine("Found {0} items", items.Count);
            IList<District> districts;
            Console.WriteLine("Set district and region Id for items");
            var time = DateTime.Now;
            using (var session = NHibernateHelper.OpenSession())
            {
                districts = session.QueryOver<District>().List();
            }
            LoadDefaultReplacer();
            for (int i = 0, j = 0; i < items.Count; i++)
            {
                bool found = false;
                for (j = 0; j < districts.Count; j++)
                {
                    found = false;
                    foreach (var bind in bindingDistrcitId)
                    {
                        if (items[i].additionalDistrictId == bind.s1)
                        {
                            found = true;
                            items[i].additionalDistrictId = bind.s2;
                            break;
                        }
                    }
                    if (found)
                        continue;
                    if (items[i].additionalDistrictId == districts[j].DistrictAddID)
                    {
                        items[i].districtId = districts[j].DistrictID;
                        items[i].regionId = districts[j].Region.RegionID;
                        break;
                    }
                }
                if (j == districts.Count)
                {
                    Console.WriteLine("Warning: {0} element doesn't have districtId");
                }
            } var t = (DateTime.Now - time);
            Console.WriteLine("Done for {0}.{1} seconds (in ms - {2})", t.Seconds, t.Milliseconds, t.TotalMilliseconds);
            
            using (var session = NHibernateHelper.OpenSession())
            {
                time = DateTime.Now;
                using (var transaction = session.BeginTransaction())
                {
                    bool haveError = false;
                    int added = 0;
                    Console.WriteLine("Starting transaction");
                    foreach (var item in items)
                        try
                        {
                            session.Save(item);
                            added++;
                        }
                        catch
                        {
                            Console.WriteLine("Error with item {0} {1}", item.additionalDistrictId, item.additionalRegionId);
                            haveError = true;
                        }
                    Console.WriteLine("Done for {0} seconds", (DateTime.Now - time).Seconds);
                    if (haveError)
                    {
                        Console.WriteLine("Can't add to database. Resolve erros and try again.\nPress any key to continue");
                        Console.ReadKey();

                    }
                    else
                    {
                        Console.WriteLine("Adding {0} records to DB", added);
                        transaction.Commit();
                        Console.WriteLine("{0} done", folderName);
                    }
                }

            }

            Console.WriteLine("========================================");

        }

        static void Main(string[] args)
        {
            //LoadToDb("2010");  // Done
            //LoadToDb("2011"); // Done 
            LoadToDb("2013");
        }
    }
}
//class program
//{
//static void Main()
//{
//    return;
//}
//}
