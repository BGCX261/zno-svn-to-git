﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Zno.Web
{
    // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
    // см. по ссылке http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
            "jsData", // Имя маршрута
            "js/data/{pageIndex}", // URL-адрес с параметрами
            new { controller = "School", action = "GetDataJs", pageIndex = 0 },
            new { pageIndex = @"\d+" }
         );
            routes.MapRoute(
             "jsSearchCity1", // Имя маршрута
             "schools/city/{CityName}", // URL-адрес с параметрами
             new { controller = "School", action = "GetDataCity", CityName = "" }
         );

            routes.MapRoute(
             "jsSearchDistrict", // Имя маршрута
             "schools/DistrictList/{RegionID}", // URL-адрес с параметрами
             new { controller = "School", action = "GetDistrictList", RegionID = 0 }
         );
            routes.MapRoute(
             "jsSearchCity", // Имя маршрута
             "schools/CityList/{RegionID}", // URL-адрес с параметрами
             new { controller = "School", action = "GetCityList", RegionID = 0 }
         );
            routes.MapRoute(
             "jsSearchByDistrict", // Имя маршрута
             "schools/ByDistrictList/{RegionID}/{DistrictID}/{CityName}/{pageindex}", // URL-адрес с параметрами
             new { controller = "School", action = "GetSchoolSByDistrict", RegionID = 0, DistrictID = 0, CityName = "", pageindex = 0 }

         );
            routes.MapRoute(
             "jsSearchByFullSchoolName", // Имя маршрута
             "schools/ByFullSchoolName/{RegionID}/{DistrictID}/{CityName}/{SearchString}/{CountParameter}", // URL-адрес с параметрами
             new { controller = "School", action = "GetSchoolSByFullSchoolName", RegionID = 0, DistrictID = 0, CityName = "", SearchString = "", CountParameter = 0 }

         );
            routes.MapRoute(
             "jsSearchSchoolById", // Имя маршрута
             "schools/BySchoolId/{pageindex}", // URL-адрес с параметрами
             new { controller = "School", action = "GetSchoolsById", pageindex = 0 }

         );
            routes.MapRoute(
                "schoolInfo",
                "school/{Id}",
                new { controller = "School", action = "School" },
                new { Id = @"\d+" }
                );
            routes.MapRoute(
                "schoolPostInfo",
                "school/post/",
                new { controller = "School", action = "GetSchoolInfoById", Id = "0" },
                new { Id = @"\d+" }
                );
            routes.MapRoute(
                "schoolBag",
                "schoolsBag/{ids}",
                new { controller = "School", action = "Compare", ids = "" },
                new { ids = @"[\d;]+"}
                );
            routes.MapRoute(
                "SearchSchoolInfoBag",
                "SearchSchoolInfoBag",
                new { controller = "School", action = "GetSchoolInfo" }
                );
            routes.MapRoute(
             "Default", // Имя маршрута
             "{controller}/{action}/{id}", // URL-адрес с параметрами
             new { controller = "School", action = "Index", id = 0 }
         );
            
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}