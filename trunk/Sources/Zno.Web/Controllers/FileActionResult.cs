﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Zno.Web.Controllers
{
    public class FileActionResult : ActionResult
    {
        public string FilePath { get; private set; }

        public FileActionResult(string filePath)
        {
            this.FilePath = filePath;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.WriteFile(FilePath);

            context.HttpContext.Response.ContentType = "text/html";
        }
    }
}