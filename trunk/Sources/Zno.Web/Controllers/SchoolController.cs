﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zno.Business;
namespace Zno.Web.Controllers
{
    public class SchoolController : Controller
    {
        //
        // GET: /School/
        [HttpGet]
        public ActionResult Index(int pageIndex = 0)
        {
            return View();
        }
        public ActionResult School(int id)
        {
            if (!SchoolHelper.IsSchoolIdValid(id))
                return View("SchoolNotFound");
            return View(id);
        }

        [HttpGet]
        public ActionResult Indexjs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetDataCity(string CityName)
        {
            var data = RatingMethod.GetByCity(CityName);
            return Json(data);
        }

        [HttpPost]
        public ActionResult GetDistrictList(string RegionID)
        {
            var data = RatingMethod.GetDistrictList(short.Parse(RegionID));
            return Json(data);
        }

        [HttpPost]
        public ActionResult GetCityList(string RegionID)
        {
            var data = RatingMethod.GetCityList(short.Parse(RegionID));
            return Json(data);
        }

        [HttpPost]
        public ActionResult GetSchoolSByDistrict(short RegionID, int DistrictID, string CityName, int pageindex)
        {
            var response = RatingMethod.GetSchoolsByDistrict(RegionID, DistrictID, CityName, pageindex);
            return Json(response);
        }

        [HttpPost]
        public ActionResult GetSchoolSByFullSchoolName(short RegionID, int DistrictID, string CityName, string SearchString, int CountParameter)
        {
            var response = SearchSchoolNameLucene.SearchSchoolbyName(RegionID, DistrictID, CityName, SearchString, CountParameter);
            return Json(response);
        }

        [HttpPost]
        public ActionResult GetSchoolsById(ICollection<int> SchoolIdList, int pageindex)
        {
            var response = RatingMethod.GetSchoolsByID(SchoolIdList.Skip(16 * pageindex).Take(16).ToList(), pageindex);
            return Json(response);
        }

        [HttpPost]
        public ActionResult GetSchoolInfoById(int Id)
        {
            var response = RatingMethod.GetSchoolInfoById(Id);
            return Json(response);
        }

        public ActionResult Compare(string ids)
        {
            var list = ids.Split(';');
            var listint = new List<int>();
            int tryparse;
            foreach (var l in list)
            {
                if (Int32.TryParse(l, out tryparse))
                {
                    if (SchoolHelper.IsSchoolIdValid(tryparse))
                        listint.Add(tryparse);
                }
            }

            listint = listint.Distinct().ToList();
            if (listint.Count() > 4 || listint.Count() < 2) throw new Exception("SchoolsBag Over");
            var args = Newtonsoft.Json.JsonConvert.SerializeObject(RatingMethod.GetBagRecords(listint));
            return View(model: args);
        }

        #region Cache
        public ActionResult test(string id)
        {
            if (!RenderHelper.HasRenderedVersion(id))
                RenderHelper.Render(this, id);

            return new FileActionResult(RenderHelper.GetRenderedVersionPath(id));
        }

        private static class RenderHelper
        {
            public static bool HasRenderedVersion(string id)
            {
                return System.IO.File.Exists(GetRenderedVersionPath(id));
            }

            public static void Render(Controller controller, string id)
            {
                using (var writer = new StringWriter())
                {
                    ViewEngineResult result = ViewEngines
                              .Engines
                              .FindView(controller.ControllerContext, "IdView", null);

                    var viewPath = ((WebFormView)result.View).ViewPath;
                    var view = new WebFormView(controller.ControllerContext, viewPath);
                    var vdd = new ViewDataDictionary<string>(id);
                    var viewCxt = new ViewContext(controller.ControllerContext, view, vdd, new TempDataDictionary(), writer);
                    viewCxt.View.Render(viewCxt, writer);
                    System.IO.File.WriteAllText(GetRenderedVersionPath(id), writer.ToString());
                }
            }

            public static string GetRenderedVersionPath(string id)
            {
                return Path.Combine("folder", id);
            }
        }
        #endregion
    }
}
