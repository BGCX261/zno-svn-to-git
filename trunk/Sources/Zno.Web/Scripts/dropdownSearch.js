﻿$(document).ready(function () {
    $('#search').keydown(function (event) {
        if (event.which == 40) {
            var list = $('#drop_search').find('li');
            if (list.length == 0) { return; }
            else {
                var selected = $('.dropSchool').filter(function () {
                    return $(this).css('color') == 'rgb(255, 255, 255)';
                });
                var index = $(list).index(selected) + 1;
                $(list[index - 1]).css('color', 'black');
                $(list[index - 1]).css('background-color', 'white');
                if (index > list.length - 1) {
                    $('#drop_search').css('width', '100%');
                    $('.dropSchool').css('height', '20px');
                    $('#drop_search').css('padding-bottom', '5px');
                }
                else {
                    $('#drop_search').css('width', '400%');
                    $('.dropSchool').css('height', '');
                    $('#drop_search').css('height', '');
                    $('#drop_search').css('padding-bottom', '');
                    $(list[index]).css('color', 'white');
                    $(list[index]).css('background-color', 'black');
                }
            }
        }
        else if (event.which == 38) {
            debugger;
            var list = $('#drop_search').find('li');
            if (list.length == 0) { return; }
            else {
                var selected = $('li').filter(function () {
                    return $(this).css('color') == 'rgb(255, 255, 255)';
                });
                var index = $(list).index(selected) - 1;

                $(list[index + 1]).css('color', 'black');
                $(list[index + 1]).css('background-color', 'white');
                if (index == -2) {
                    index = list.length - 1;
                    $('#drop_search').css('width', '400%');
                    $('.dropSchool').css('height', '');
                    $('#drop_search').css('height', '');
                    $('#drop_search').css('padding-bottom', '');
                    $(list[index]).css('color', 'white');
                    $(list[index]).css('background-color', 'black');
                }
                else if (index < 0) {
                    $('#drop_search').css('width', '100%');
                    $('.dropSchool').css('height', '20px');
                    $('#drop_search').css('padding-bottom', '5px');
                }
                else {
                    $('#drop_search').css('width', '400%');
                    $('.dropSchool').css('height', '');
                    $('#drop_search').css('height', '');
                    $('#drop_search').css('padding-bottom', '');
                    $(list[index]).css('color', 'white');
                    $(list[index]).css('background-color', 'black');
                }
            }
        }
        else if (event.which == 27) {
            $('.dropSchool').css('color', 'black');
            $('.dropSchool').css('background-color', 'white');
            $('#drop_search').css('width', '100%');
            $('.dropSchool').css('height', '20px');
            $('#drop_search').css('padding-bottom', '5px');
            $('#drop_search').css('visible', '')
            $('#drop_search').css('display', 'none');
        }
    });
    var pageX;
    var pageY;
    $('#drop_search').mousemove(function (e) {
        if (pageX == e.pageX && pageY == e.pageY) return;
        if (!e) e = window.event;
        var li = e.target || e.srcElement;
        if (li.tagName == 'LI') {
            $('.dropSchool').css('color', 'black');
            $('.dropSchool').css('background-color', 'white');
            $(li).css('color', 'white');
            $(li).css('background-color', 'black');
        }
        pageX = e.pageX;
        pageY = e.pageY;
    });
    $(document).click(function (e) {
        if (!e) e = window.event;
        var li = e.target || e.srcElement;
        if (li.tagName == 'LI' || li.tagName == 'UL') { return; }
        else if (li.tagName == 'INPUT') {
            if ($('#drop_search').find('li').length != 0) {
                $('#drop_search').css('display', '');
            }
        }
        $('.dropSchool').css('color', 'black');
        $('.dropSchool').css('background-color', 'white');
        $('#drop_search').css('width', '100%');
        $('.dropSchool').css('height', '20px');
        $('#drop_search').css('padding-bottom', '5px');
    })
});