﻿BagViewModelOld = (function () {
    var model = function (Schools) {
        debugger;
        var self = this;
        this.colors = ko.observableArray(["rgb(23, 103, 153)", "#1c7ebb", "rgb(66, 164, 187)", "rgb(120, 214, 199)"]);
        this.left = ko.observable(0);
        this.SubjectVisible = ko.observable(false);
        this.OpenSubjects = function () {
            debugger;
            self.SubjectVisible(!self.SubjectVisible());
        }
        var json = JSON.parse(Schools);
        if (json.School.length == 2) self.left(25);
        else if (json.School.length == 3) self.left(12.5);
        else self.left(0);
        for (var i = 0; i < json.School.length; i++) {
            json.School[i].Name = json.School[i].Name.split('&').join('"');
            json.School[i].Address = json.School[i].Address.split('&').join('"');
            json.School[i].Description = json.School[i].Description.split('&').join('"');
            json.School[i].Color = self.colors()[i];
        }
        this.SchoolsBag = ko.observable(json);
    };
    return model;
})();