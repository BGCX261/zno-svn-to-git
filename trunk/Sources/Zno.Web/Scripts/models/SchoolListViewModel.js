﻿SchoolListViewModel = (function () {
    var model = function (getDataUrl, searchDataUrl, searchDataUrlByOptions, searchCity, jsSearchString, jsSchoolsById) {
        var self = this;
        //url parser
        this.urlStandard = function () {
            document.location.hash = "#Region:undefined;District:undefined;City:undefined;Search:;Bags:";
        }
        /* check url */
        var Region;
        var District;
        var City;
        var Bag;
        var DistrictList;
        var CityList;
        //function parser
        this.ParserUrl = function () {
            this.isValidUrl = false;
            var url = document.location.hash;
            if (!(url.indexOf('#Region:') != -1 && url.indexOf('District:') != -1 && url.indexOf('City:') != -1 && url.indexOf('Search:') != -1 && url.indexOf('Bags:') != -1)) {
                return false;
            }
            else {
                Region = url.substr(url.indexOf('Region') + 7, url.indexOf(';District') - url.indexOf('Region') - 7);
                if (Region == 'undefined') {
                    District = url.substr(url.indexOf(';District') + 10, url.indexOf(';City') - url.indexOf(';District') - 10);
                    City = url.substr(url.indexOf(';City') + 6, url.indexOf(';Search') - url.indexOf(';City') - 6);
                    if (District == 'undefined' && City == 'undefined') {
                        if (/Bags:(\S*)/.exec(url)[0][/Bags:(\S*)/.exec(url)[0].length - 1] != ',' && /Bags:(\S*)/.exec(url)[0][/Bags:(\S*)/.exec(url)[0].length - 1] != ':') return false;
                        Bag = /Bags:(\S*)/.exec(url)[0].substr(5, /Bags:(\S*)/.exec(url)[0].length - 6).split(',');
                        if (Bag == null || Bag.length == 0) return true;
                        var ifalse = false;
                        for (var j = 0; j < Bag.length; j++) {
                            if (Bag[j] != '') ifalse = true;
                        }
                        if (!ifalse) return true;
                        if (Bag.length > 4) return false;
                        for (var count = 0; count < Bag.length; count++) {
                            if (!(isDigit(Bag[count]) && Bag[count] > 0 && Bag[count] < 7802)) {
                                return false;
                            }
                            for (var count2 = count + 1; count2 < Bag.length; count2++) {
                                if (Bag[count] == Bag[count2]) {
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                    else return false;
                }
                else {
                    for (var i = 0; i < self.RegionList.length; i++) {
                        if (self.RegionList[i].Id == Region) {
                            self.isValidUrl = true;
                        }
                    }
                    if (!self.isValidUrl) return false;
                }
                if (self.isValidUrl) {
                    self.isValidUrl = false;
                    District = url.substr(url.indexOf(';District') + 10, url.indexOf(';City') - url.indexOf(';District') - 10);
                    City = url.substr(url.indexOf(';City') + 6, url.indexOf(';Search') - url.indexOf(';City') - 6);

                    $.ajax({
                        type: "POST",
                        url: searchDataUrl,
                        dataType: "json",
                        traditional: true,
                        async: false,
                        data: { RegionID: Region },
                        success: function (data) {
                            DistrictList = data;
                            $.ajax({
                                type: "POST",
                                url: searchCity,
                                dataType: "json",
                                traditional: true,
                                async: false,
                                data: { RegionID: Region },
                                success: function (data) {
                                    CityList = data;
                                }
                            });
                        }
                    });
                    if (District == 'undefined') self.isValidUrl = true;
                    else {
                        for (var d = 0; d < DistrictList.length; d++) {
                            if (DistrictList[d].Id == District) self.isValidUrl = true;
                        }

                        if (self.isValidUrl) {
                            if (City != 'undefined')
                                return false;
                        }
                        else return false;
                    }
                    if (City == 'undefined') {
                        if (/Bags:(\S*)/.exec(url)[0][/Bags:(\S*)/.exec(url)[0].length - 1] != ',' && /Bags:(\S*)/.exec(url)[0][/Bags:(\S*)/.exec(url)[0].length - 1] != ':') return false;
                        Bag = /Bags:(\S*)/.exec(url)[0].substr(5, /Bags:(\S*)/.exec(url)[0].length - 6).split(',');
                        if (Bag == null || Bag.length == 0) return true;
                        var ifalse = false;
                        for (var j = 0; j < Bag.length; j++) {
                            if (Bag[j] != '') ifalse = true;
                        }
                        if (!ifalse) return true;
                        if (Bag.length > 4) return false;
                        for (var count = 0; count < Bag.length; count++) {
                            if (!(isDigit(Bag[count]) && Bag[count] > 0 && Bag[count] < 7802)) {
                                return false;
                            }
                            for (var count2 = count + 1; count2 < Bag.length; count2++) {
                                if (Bag[count] == Bag[count2]) {
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                    else {
                        self.isValidUrl = false;
                        for (var s = 0; s < CityList.length; s++) {
                            if (CityList[s].CityName == City) self.isValidUrl = true;
                        }
                        if (self.isValidUrl) {
                            debugger;
                            var a = /Bags:(\S*)/.exec(url)[0];
                            if (/Bags:(\S*)/.exec(url)[0][/Bags:(\S*)/.exec(url)[0].length - 1] != ',' && /Bags:(\S*)/.exec(url)[0][/Bags:(\S*)/.exec(url)[0].length - 1] != ':') return false;
                            Bag = /Bags:(\S*)/.exec(url)[0].substr(5, /Bags:(\S*)/.exec(url)[0].length - 6).split(',');
                            if (Bag == null || Bag.length == 0) return true;
                            var ifalse = false;
                            for (var j = 0; j < Bag.length; j++) {
                                if (Bag[j] != '') ifalse = true;
                            }
                            if (!ifalse) return true;
                            if (Bag.length > 4) return false;
                            for (var count = 0; count < Bag.length; count++) {
                                if (!(isDigit(Bag[count]) && Bag[count] > 0 && Bag[count] < 7802)) {
                                    return false;
                                }
                                for (var count2 = count + 1; count2 < Bag.length; count2++) {
                                    if (Bag[count] == Bag[count2]) {
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                        else return false;
                    }
                }
                else {
                    return false;
                }
            }
        }
        //load by hash from url
        this.LoadByHash = function () {
            self.IsHash = true;
            Bagarray = [];
            if (Bag.length != 0) {
                for (var b = 0; b < Bag.length; b++) {
                    if (Bag[b] != '' && isDigit(Bag[b])) {
                        Bagarray.push(Bag[b]);
                    }
                }
                if (Bagarray.length != 0) {
                    $.ajax({
                        type: "POST",
                        url: jsSchoolsById,
                        dataType: "json",
                        traditional: true,
                        async: false,
                        data: { SchoolIdList: Bagarray, pageindex: 0 },
                        success: function (data) {
                            $.each(data, function (index, obj) {
                                self.AddToBag(new SchoolViewModel(obj));
                            });
                        }
                    });
                }
            }
            if (Region != 'undefined') {
                self.selectedRegionID(Region);
                self.IsRegionLoaded(true);
                var modelD = [];
                for (var i = 0; i < DistrictList.length; i++) {
                    modelD.push(new DistrictViewModel(DistrictList[i]));
                }
                self.DistrictList(modelD);
                var modelC = [];
                for (var i = 0; i < CityList.length; i++) {
                    modelC.push(new CityViewModel(CityList[i]));
                }
                self.CityList(modelC);
            }
            if (District != 'undefined') {
                self.selectedDistrictID(District);
            }
            if (City != 'undefined') {
                self.selectedCityName(City);
            }
            var Search = /;Search:([\s\S]*);Bags:/.exec(document.location.hash)[0].substr(8, /;Search:([\s\S]*);Bags:/.exec(document.location.hash)[0].length - 14);
            if (Search != undefined && Search.length != 0 && !ispunctonly(Search)) {
                //$('#search').val(Search);
                self.IsLoaded(true);
                self.SearchSchool(Search);
                self.enterPressed();
            }
            else {
                self.LoadByVse();
            }
            setTimeout(function () { self.IsHash = false; }, 1000);
        }
        //If hash parser work
        this.IsHash = false;
        /* Basic config */
        this.currentPage = 0;
        //If region list loaded
        this.IsRegionLoaded = ko.observable(false);
        /*status of div block*/
        this.IsLoaded = ko.observable(false);
        //Array of schools in main page
        this.Schools = ko.observableArray([]);
        /* Region list for filter */
        this.RegionList = [{ Name: "Автономна республіка Крим", Id: '1' }, { Name: "Вінницька область", Id: '2' }, { Name: "Волинська область", Id: '3' },
        { Name: "Дніпропетровська область", Id: '4' }, { Name: "Донецька область", Id: '5' }, { Name: "Житомирська область", Id: '6' },
        { Name: "Закарпатська область", Id: '7' }, { Name: "Запорізька область", Id: '8' }, { Name: "Івано-Франківська область", Id: '9' },
        { Name: "Київська область", Id: '10' }, { Name: "Кіровоградська область", Id: '11' }, { Name: "Луганська область", Id: '12' },
        { Name: "Львівська область", Id: '13' }, { Name: "Миколаївська область", Id: '14' }, { Name: "Одеська область", Id: '15' },
        { Name: "Полтавська область", Id: '16' }, { Name: "Рівненська область", Id: '17' }, { Name: "Сумська область", Id: '18' },
        { Name: "Тернопільська область", Id: '19' }, { Name: "Харківська область", Id: '20' }, { Name: "Херсонська область", Id: '21' },
        { Name: "Хмельницька область", Id: '22' }, { Name: "Черкаська область", Id: '23' }, { Name: "Чернівецька область", Id: '24' },
        { Name: "Чернігівська область", Id: '25' }, { Name: "м.Київ", Id: '26' }, { Name: "м.Севастополь", Id: '27' }, ];
        /* Disrtict, City list for filter */
        this.DistrictList = ko.observableArray([]);
        this.CityList = ko.observableArray([]);
        /*Selected points in selects*/
        this.selectedRegionID = ko.observable();
        this.selectedDistrictID = ko.observable();
        this.selectedCityName = ko.observable();
        //Search string in input
        this.SearchSchool = ko.observable("");
        //List ofTips below input
        this.SearchSchoolList = ko.observableArray([]);
        //Visible if query give a result
        this.FindSchools = ko.observable(false);
        //Key whitch scroll up
        this.UpKeyVisible = ko.observable(false);
        //Scrool in press key Up
        this.ScrollUp = function () {
            $('body,html').animate({ scrollTop: 0 }, 500);
            return false;
        }
        //Array for bag
        this.Bag = ko.observableArray([]);
        //Bag Submit Click
        this.BagSubmitClick = function () {
            var start = 'schoolsBag/';
            for (var i = 0; i < self.Bag().length; i++) {
                start += self.Bag()[i].Id + ';';
            }
            start += document.location.hash;
            window.open(start, '_self');
        }
        //Text in button for bug
        this.AddBugText = "До порівняння";
        this.RemoveBugText = "Видалити з порівняння";
        //Visible message if bag over
        this.isBagOver = ko.observable(false);
        //Visible Bag
        this.IsBagVisible = ko.computed(function () {
            if (self.Bag().length > 0) return true;
            return false;
        }, this)
        //visible button for submit bug
        this.bagsubmitvisible = ko.observable(false);
        //Add to bag
        this.AddToBag = function (schoolViewModel) {
            var index = self.Bag.indexOf(schoolViewModel);
            if (index != -1) {

                schoolViewModel.textInAddToBag(self.AddBugText);
                self.Bag.remove(schoolViewModel);
                schoolViewModel.BagButtonClass('openButton');
                var regexp = new RegExp(schoolViewModel.Id + ",");
                if (regexp.test(document.location.hash)) {
                    document.location.hash = document.location.hash.replace(regexp.exec(document.location.hash)[0], '');
                }
                if (self.Bag().length < 4) {
                    $('.openButton').prop('title', self.AddBugText);
                    $('.openButton').removeClass('overButton');
                }
            }
            else { // add to bug
                if (self.Bag().length < 4) {
                    self.Bag.push(schoolViewModel);
                    schoolViewModel.textInAddToBag(self.RemoveBugText);
                    schoolViewModel.BagButtonClass('closeButton');
                    if (!self.IsHash) {
                        document.location.hash += (schoolViewModel.Id + ',');
                    }
                    if (self.Bag().length == 4) {
                        $('.openButton').prop('title', 'Список порівнянь заповнений');
                        $('.openButton').addClass('overButton');
                    }
                }
                else return;
            }
            if (self.Bag().length > 1) self.bagsubmitvisible(true);
            else self.bagsubmitvisible(false);
        };
        //ref to page schoolinfo
        this.cardClicked = function (model, e) {
            if (e.target.className != "closeButton" && e.target.className != "openButton" && $(e.target).prop('title') != 'Список порівнянь заповнений') {
                window.open('/school/' + model.Id+window.location.hash, '_self');
            }
        }
        //Remove from bag
        this.RemoveFromBag = function (schoolViewModel) {
            schoolViewModel.textInAddToBag(self.AddBugText);
            self.Bag.remove(schoolViewModel);
            schoolViewModel.BagButtonClass('openButton');
            var regexp = new RegExp(schoolViewModel.Id + ",");
            if (regexp.test(document.location.hash)) {
                document.location.hash = document.location.hash.replace(regexp.exec(document.location.hash)[0], '');
            }
            if (self.Bag().length < 2) self.bagsubmitvisible(false);
            if (self.Bag().length < 4) {
                $('.openButton').prop('title', self.AddBugText);
                $('.openButton').removeClass('overButton');
            }
        }
        //If Loading right now
        this.Loading = false;
        //When dropdown enable in focus
        this.enable = function () {
            self.focusul = true;
            $('#drop_search').css('width', '400%');
            $('.dropSchool').css('height', '');
            $('#drop_search').css('height', '');
            $('#drop_search').css('padding-bottom', '');
        }
        //When dropdown disable
        this.disable = function () {
            $('.dropSchool').css('color', 'black');
            $('.dropSchool').css('background-color', 'white');
            self.focusul = false;
            $('#drop_search').css('width', '100%');
            $('.dropSchool').css('height', '20px');
            $('#drop_search').css('padding-bottom', '5px');
        }
        //If any element in dropdown in focus
        this.isSelected = ko.observable(false);
        //When dropdown in focus
        this.focusul = false;
        //If request get nothing result
        this.isnotfound = ko.observable(false);
        //When loading start and end
        this.IsLoaded.subscribe(function (newValue) {
            if (newValue == false) {
                $('#search').attr('readonly', 'readonly');
                $('#RegionSelect').attr('disabled', 'disabled');
                $('#DistrictSelect').attr('disabled', 'disabled');
                $('#CitySelect').attr('disabled', 'disabled');
                self.isnotfound(false);
            }
            else {
                $('#search').removeAttr('readonly');
                $('#RegionSelect').removeAttr('disabled');
                $('#DistrictSelect').removeAttr('disabled');
                $('#CitySelect').removeAttr('disabled');
            }
        });
        //Visible of dropdown
        this.isVisbleDropdown = ko.computed(function () {
            return self.FindSchools() && (self.isSelected() || self.focusul);
        }, this);
        //Value in input with timer for subscribe
        this.delayedValue = ko.computed(this.SearchSchool)
        .extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 600 } });
        //Take one School by click
        self.takeOneSchool = function (e, value) {
            if (!value) {
                if (!e) e = window.event;
                var val = e.target || e.srcElement;
                if (val.value != undefined) {
                    var func = val.innerText || val.innerHTML || val.textContent;
                    if ($('#search').val() == func) {
                        self.FindSchools(false);
                        self.focusul = false;
                        self.isSelected(false);
                    };
                    self.takeOneSchoolLoad(val.value);
                    self.SearchSchool(func);
                    self.FindSchools(false);
                    self.focusul = false;
                    self.isSelected(false);

                }
            }
            else {
                var func = value.innerText || value.innerHTML || value.textContent;
                if ($('#search').val() == func) {
                    self.FindSchools(false);
                    self.focusul = false;
                    self.isSelected(false);
                };
                self.takeOneSchoolLoad(value.value);
                self.SearchSchool(func);
                self.FindSchools(false);
                self.focusul = false;
                self.isSelected(false);
            }
        }

        //Load schools from seach string by id
        self.LoadSchoolsById = function (obj) {
            var intArray = new Array();
            if (obj != undefined) {
                intArray.push(obj);
            }
            else {
                for (var i = 0; i < self.SearchSchoolList().length; i++) {
                    intArray[i] = self.SearchSchoolList()[i].Id;
                }
                if (intArray.length == 0) {
                    self.IsLoaded(true);
                    self.isnotfound(true);
                    return;
                }
                else self.isnotfound(false);
            }
            $.ajax({
                type: "POST",
                url: jsSchoolsById,
                dataType: "json",
                traditional: true,
                data: { SchoolIdList: intArray, pageindex: self.currentPage++ },
                success: function (data) {
                    var models = self.Schools();
                    if (data.length < 16) { self.isEndofRecords = true; }
                    $.each(data, function (index, obj) {
                        models.push(new SchoolViewModel(obj));
                    });
                    self.Schools(models);
                    if (self.Bag().length > 3) {
                        //$('.openButton').css('display', 'none');
                        $('.openButton').prop('title', 'Список порівнянь заповнений');
                        $('.openButton').addClass('overButton');
                    }
                    // For bag unique
                    var arrSch = [];
                    for (var i = 0; i < self.Schools().length; i++) {
                        arrSch.push(self.Schools()[i].Id);
                    }
                    for (var i = 0; i < self.Bag().length; i++) {
                        var index = arrSch.indexOf(self.Bag()[i].Id);
                        if (index != -1) {
                            self.Bag.splice(i, 1, self.Schools()[index]);
                            self.Schools()[index].BagButtonClass('closeButton');
                            //self.Schools()[index].textInAddToBag(self.RemoveBugText);
                        }
                    }
                    $('.closeButton').css('display', '');
                    __Resize();
                    self.IsLoaded(true);
                    self.used = false;
                    self.FindSchools(false);
                }
            });
        }
        //Loading for take one School
        this.takeOneSchoolLoad = function (obj) {
            self.SearchSchoolList([]);
            self.IsLoaded(false);
            self.isEndofRecords = false;
            self.used = true;
            self.Schools([]);
            self.currentPage = 0;
            self.LoadSchoolsById(obj);
        }
        //press Enter
        this.enterPressed = function () {
            if (self.IsLoaded()) {
                var selected = $('.dropSchool').filter(function () {
                    return $(this).css('color') == 'rgb(255, 255, 255)';
                });
                if (selected.length != 0) {
                    self.takeOneSchool(undefined, selected[0]);
                }
                else {
                    if (ispunctonly(self.SearchSchool()) == false) {
                        self.Schools([]);
                        self.IsLoaded(false);
                        setTimeout(function () {
                            self.SearchSchoolList([]);
                            self.isEndofRecords = false;
                            self.used = true;
                            self.currentPage = 0;
                            self.LoadBySearchString(1, self.LoadSchoolsById);
                        }, 1500);
                    }
                }
            }
        }
        //Skip filter
        this.skipPressed = function () {
            self.SearchSchool("");
            if (/#Region:(\w*);/.test(document.location.hash)) {
                document.location.hash = document.location.hash.replace(/#Region:(\w*);/.exec(document.location.hash)[0], '#Region:undefined;');
            }
            if (/;City:(.*);Search:/.test(document.location.hash)) {
                document.location.hash = document.location.hash.replace(/;City:(.*);Search:/.exec(document.location.hash)[0], ';City:undefined;Search:');
            }
            if (/;District:(\w*);/.test(document.location.hash)) {
                document.location.hash = document.location.hash.replace(/;District:(\w*);/.exec(document.location.hash)[0], ';District:undefined;');
            }
            self.isEndofRecords = false;
            if (self.selectedRegionID() != undefined) {
                self.selectedRegionID(undefined);
            }
            else {
                self.Schools([]);
                self.currentPage = 0;
                self.LoadByVse();
            }
        }


        //Load tips by SearchString 
        self.LoadBySearchString = function (parameter, callback) {
            var execureCallback = function () {
                if (typeof (callback) == 'function') {
                    callback.call();
                }
            };
            if (self.SearchSchool() != undefined && self.SearchSchool().length != 0 && ispunctonly(self.SearchSchool()) == false) {
                $.post(jsSearchString, { RegionID: self.selectedRegionID() == undefined ? 0 : self.selectedRegionID, DistrictID: self.selectedDistrictID() == undefined ? 0 : self.selectedDistrictID, CityName: self.selectedCityName() == undefined ? "" : self.selectedCityName, SearchString: self.SearchSchool().replace('<', '').replace('>', ''), CountParameter: parameter }, function (data) {
                    var models = self.SearchSchoolList();
                    if (data.length < 1) { self.FindSchools(false); }
                    else { self.FindSchools(true); }
                    $.each(data, function (index, obj) {
                        models.push(new SearchSchoolModel(obj));
                    });
                    $('#drop_search').css('width', '100%');
                    $('.dropSchool').css('height', '20px');
                    $('#drop_search').css('padding-bottom', '5px');
                    if (parameter == 0) self.SearchSchoolList(models);
                    execureCallback();
                });
            }
            else {
                self.FindSchools(false);
                if (parameter == 1) {
                    self.LoadByVse();
                }
            }
        }
        //Load without filters
        self.LoadByVse = function () {
            if (!self.Loading) {
                self.Loading = true;
                $.post(searchDataUrlByOptions, { RegionID: self.selectedRegionID() == undefined ? 0 : self.selectedRegionID, DistrictID: self.selectedDistrictID() == undefined ? 0 : self.selectedDistrictID, CityName: self.selectedCityName() == undefined ? "" : self.selectedCityName, pageindex: self.currentPage++ }, function (data) {
                    var models = self.Schools();
                    if (data.length < 16) { self.isEndofRecords = true; }
                    $.each(data, function (index, obj) {
                        models.push(new SchoolViewModel(obj));
                    });
                    if (models.length == 0) {
                        self.isnotfound(true);
                        self.Loading = false;
                        return;
                    }
                    else self.isnotfound(false);
                    self.Schools(models);
                    if (self.Bag().length > 3) {
                        //$('.openButton').css('display', 'none');
                        $('.openButton').prop('title', 'Список порівнянь заповнений');
                        $('.openButton').addClass('overButton');

                    }
                    // For bag unique
                    var arrSch = [];
                    for (var i = 0; i < self.Schools().length; i++) {
                        arrSch.push(self.Schools()[i].Id);
                    }
                    for (var i = 0; i < self.Bag().length; i++) {
                        var index = arrSch.indexOf(self.Bag()[i].Id);
                        if (index != -1) {
                            self.Bag.splice(i, 1, self.Schools()[index]);
                            self.Schools()[index].BagButtonClass('closeButton');
                            //self.Schools()[index].textInAddToBag(self.RemoveBugText);
                        }
                    }
                    $('.closeButton').css('display', '');
                    __Resize();
                    self.IsLoaded(true);
                    self.used = false;
                    self.Loading = false;
                });
            }
        }
        //Get tips for dropdown
        this.delayedValue.subscribe(function (newValue) {
            if (!self.IsHash) {
                if (/;Search:([\s\S]*);Bags:/.test(document.location.hash)) {
                    document.location.hash = document.location.hash.replace(/;Search:([\s\S]*);Bags:/.exec(document.location.hash)[0], ';Search:' + newValue + ';Bags:');
                }
                self.SearchSchoolList([]);
                self.LoadBySearchString(0, 0);
            }
        });
        /*Event when new region selected*/
        this.selectedRegionID.subscribe(function (newValue) {
            debugger;
            if (!self.IsHash) {
                self.isTwoLoaders = true;
                self.used = true;

                //self.UnSubcscribeselectedCityName();


                self.Schools([]);
                self.DistrictList([]);
                self.CityList([]);
                self.isEndofRecords = false;
                if (newValue == undefined) {
                    self.IsRegionLoaded(false);
                }
                if (/#Region:(\w*);/.test(document.location.hash)) {
                    document.location.hash = document.location.hash.replace(/#Region:(\w*);/.exec(document.location.hash)[0], '#Region:' + newValue + ';');
                }
                if (/;City:([\s\S]*);Search:/.test(document.location.hash)) {
                    document.location.hash = document.location.hash.replace(/;City:([\s\S]*);Search:/.exec(document.location.hash)[0], ';City:undefined;Search:');
                }
                if (/;District:(\w*);/.test(document.location.hash)) {
                    document.location.hash = document.location.hash.replace(/;District:(\w*);/.exec(document.location.hash)[0], ';District:undefined;');
                }
                if (newValue != undefined) {
                    $.post(searchDataUrl, { RegionID: self.selectedRegionID }, function (data) {
                        var models = [];
                        $.each(data, function (index, obj) {
                            models.push(new DistrictViewModel(obj));
                        });
                        self.DistrictList(models);
                    });
                    $.post(searchCity, { RegionID: self.selectedRegionID }, function (data) {
                        var models = [];
                        $.each(data, function (index, obj) {
                            models.push(new CityViewModel(obj));
                        });
                        self.CityList(models);
                    });
                    self.IsRegionLoaded(true);
                }
                self.currentPage = 0;
                if (self.SearchSchool() != undefined && self.SearchSchool().length != 0 && ispunctonly(self.SearchSchool()) == false) {
                    self.enterPressed();
                }
                else {
                    self.LoadByVse();
                }
                self.isTwoLoaders = false;

                //self.SubcscribeselectedCityName();
            }
        });

        /*this.subscriptionselectedCityName = null;

        this.SubcscribeselectedCityName = function () {
            if (self.subscriptionselectedCityName != null)
                return;

            self.subscriptionselectedCityName = this.selectedCityName.subscribe(function (newValue) {
                ///
            });
        };

        this.UnSubcscribeselectedCityName = function () {
            if (self.subscriptionselectedCityName != null) {
                self.subscriptionselectedCityName.dispose();
                self.subscriptionselectedCityName = null;
            }
        };

        self.SubcscribeselectedCityName();*/


        /*Event when new city selected*/
        this.selectedCityName.subscribe(function (newValue) {
            if (!self.IsHash) {
                if (!self.isTwoLoaders) {
                    self.isTwoLoaders = true;
                    self.selectedDistrictID(undefined);
                    self.used = true;
                    self.Schools([]);
                    self.SearchSchoolList([]);
                    self.isEndofRecords = false;
                    self.currentPage = 0;
                    if (/;City:([\s\S]*);Search:/.test(document.location.hash)) {
                        document.location.hash = document.location.hash.replace(/;City:([\s\S]*);Search:/.exec(document.location.hash)[0], ';City:' + newValue + ';Search:');
                    }
                    if (/;District:(\w*);/.test(document.location.hash)) {
                        document.location.hash = document.location.hash.replace(/;District:(\w*);/.exec(document.location.hash)[0], ';District:undefined;');
                    }
                    if (self.SearchSchool() != undefined && self.SearchSchool().length != 0 && ispunctonly(self.SearchSchool()) == false) {
                        self.enterPressed();
                    }
                    else {
                        self.LoadByVse();
                    }
                    self.isTwoLoaders = false;
                }
            }
        })
        /*Event when new district selected*/
        this.selectedDistrictID.subscribe(function (newValue) {
            if (!self.IsHash) {
                if (!self.isTwoLoaders) {
                    self.isTwoLoaders = true;
                    self.selectedCityName(undefined);
                    self.used = true;
                    self.Schools([]);
                    self.SearchSchoolList([]);
                    self.isEndofRecords = false;
                    self.currentPage = 0;
                    if (/;District:(\w*);/.test(document.location.hash)) {
                        document.location.hash = document.location.hash.replace(/;District:(\w*);/.exec(document.location.hash)[0], ';District:' + newValue + ';');
                    }
                    if (/;City:([\s\S]*);Search:/.test(document.location.hash)) {
                        document.location.hash = document.location.hash.replace(/;City:([\s\S]*);Search:/.exec(document.location.hash)[0], ';City:undefined;Search:');
                    }
                    if (self.SearchSchool() != undefined && self.SearchSchool().length != 0 && ispunctonly(self.SearchSchool()) == false) {
                        self.enterPressed();
                    }
                    else {
                        self.LoadByVse();
                    }
                    self.isTwoLoaders = false;
                }
            }
        });
        //For scroll blocking
        this.used = false;
        //End of records, nothing to load
        this.isEndofRecords = false;
        //Is loading for region and district or city
        this.isTwoLoaders = false;
        //Lazy loading
        this.ApplySchoolLazyLoading = function () {
            $(window).scroll(function () {
                if ($(window).scrollTop() > 500) self.UpKeyVisible(true);
                else self.UpKeyVisible(false);
                if (!self.used && !self.isEndofRecords && ($(window).scrollTop()) == ($(document).height() - $(window).height())) {
                    self.used = true;
                    if (self.SearchSchool().length == 0) {
                        self.LoadByVse();
                    }
                    else {
                        self.LoadSchoolsById();
                    }
                }
            });
        };
        //********************************************************************************************************************************
        // Ispunct
        function isPunct(aChar) {
            return (isGraph(aChar) && !(isAlnum(aChar)));
        }
        // Test for printable characters (only good up to char 127)
        function isGraph(aChar) {
            myCharCode = aChar.charCodeAt(0);

            if ((myCharCode > 32) && (myCharCode < 127)) {
                return true;
            }

            return false;
        }
        // Test for letters and digits
        function isAlnum(aChar) {
            return (isDigit(aChar) || isAlpha(aChar));
        }
        // Test for digits
        function isDigit(aChar) {
            myCharCode = aChar.charCodeAt(0);

            if ((myCharCode > 47) && (myCharCode < 58)) {
                return true;
            }

            return false;
        }
        // Test for letters (only good up to char 127)
        function isAlpha(aChar) {
            myCharCode = aChar.charCodeAt(0);

            if (((myCharCode > 64) && (myCharCode < 91)) ||
               ((myCharCode > 96) && (myCharCode < 123))) {
                return true;
            }

            return false;
        }
        // validate the search string
        var ispunctonly = function (Char) {
            for (var i = 0; i < Char.length; i++) {
                if (!isPunct(Char[i]) && Char[i] != ' ') { return false; }
            }
            return true;
        }
        //*********************************************************************************************************************************
        //start Loading
        if (!self.ParserUrl()) {
            self.urlStandard();
            self.LoadByVse();
        }
        else {
            self.LoadByHash();
        }
        //ready for the lazy loading
        self.ApplySchoolLazyLoading();
    };
    return model;
})();





//function SubModel() {
//    this.Load = function (callback) {
//        return $.post(callback);
//    }


//}

//function ff() {
//    var deferred = $.Deferred();

//    var m1 = new SubModel();
//    var m2 = new SubModel();

//    $.when(
//    m1.Load(callback),
//    m2.Load(callback)
//    ).then(function () {
//        alert('loaded');
//        deferred.resolve();
//    });

//    return deferred;
//};



//function f2() {
//    $.when(ff()).then(function () { alert(2); });
//}
