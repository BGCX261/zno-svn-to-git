﻿DistrictViewModel = (function () {

    var model = function (jsonObj) {
        var self = this;
        this.Name = jsonObj.DistrictName;
        this.Id = jsonObj.Id;
    };
    return model;
})();
