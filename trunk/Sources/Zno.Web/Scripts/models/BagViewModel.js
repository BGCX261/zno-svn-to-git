﻿BagViewModel = (function () {

    var model = function (Schools) {
        var self = this;
        this.hrefBack = ko.observable('/'+document.location.hash);
        this.colors = ko.observableArray(["white", "#f0fafe", "rgb(66, 164, 187)", "rgb(120, 214, 199)"]);
        this.left = ko.observable(0);
        this.SubjectVisible = ko.observable(false);
        this.OpenSubjects = function () {
            self.SubjectVisible(!self.SubjectVisible());
        }
        var json = JSON.parse(Schools);
        for (var i = 0; i < json.SchoolsInfo.length; i++) {
            json.SchoolsInfo[i].Name = json.SchoolsInfo[i].Name.split('&').join('"');
            json.SchoolsInfo[i].Address = json.SchoolsInfo[i].Address.split('&').join('"');
            json.SchoolsInfo[i].Description = json.SchoolsInfo[i].Description.split('&').join('"');
        }
        for (var i = 0; i < json.GeneralRating.length; i++) {
            var count = 0;
            for (var j = 0; j < json.GeneralRating[i].Points.length; j++) {
                if (json.GeneralRating[i].Points[j] == 0) {
                    json.GeneralRating[i].Points[j] = "―";
                }
                else {
                    count++;
                }
            }
            if (count == 0) {
                json.GeneralRating.splice(i, 1);
                i--;
            }
        }
        for (var i = 0; i < json.SubjectRating.length; i++) {
            var count = 0;
            for (var j = 0; j < json.SubjectRating[i].Points.length; j++) {
                if (json.SubjectRating[i].Points[j] == 0) {
                    json.SubjectRating[i].Points[j] = "―";
                }
                else {
                    count++;
                }
            }
            if (count == 0) {
                debugger;
                json.SubjectRating.splice(i, 1);
                i--;
            }
        }
        if (json.SchoolsInfo.length == 4) self.left(8);
        else if (json.SchoolsInfo.length == 3) self.left(19.5);
        else if (json.SchoolsInfo.length == 2) self.left(31);
        this.SchoolsBag = ko.observable(json);
    };
    return model;
})();