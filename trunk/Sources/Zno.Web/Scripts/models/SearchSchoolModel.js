﻿SearchSchoolModel = (function () {
    var model = function (jsonObj) {
        var self = this;
        this.Id = jsonObj.Id;
        this.Name = jsonObj.Name;
        this.elemcolor = ko.observable('black');
        this.background = ko.observable('white');
        this.focusdrop = function () { self.elemcolor('white'); self.background('black'); }
        this.nofocusdrop = function () { self.elemcolor('black'); self.background('white'); }
    };
    return model;
})();