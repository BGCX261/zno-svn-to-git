﻿SchoolViewModel = (function () {
    var self = this;
    var GetColor = function (value) {
        var a = 100, b = 150, c = 170;
        if (value == 0)
            return "red";
        if (value > a && value < b)
            return "red";
        if (value >= b && value < c)
            return "yellow";
        return "green";
    }

    var model = function (jsonObj) {
        var self = this;
        this.Id = jsonObj.Id;
        this.textInAddToBag = ko.observable('Додати до порівняння');
        this.BagButtonClass = ko.observable('openButton');
        this.NumberUkr = jsonObj.NumberUkr;
        this.NumberRegion = jsonObj.NumberRegion;
        this.NumberDistrict = jsonObj.NumberDistrict;
        this.GeneralRating = Math.round(jsonObj.GeneralRating);
        this.GeneralRatingColor = GetColor(Math.round(jsonObj.GeneralRating));
        this.Name = jsonObj.Name;
        this.Address = jsonObj.Address;
        this.Description = jsonObj.Description;

        this.districtName = jsonObj.District;
        this.regionName = jsonObj.Region;

        this.ExactSciences = Math.round(jsonObj.ExactSciences);
        this.ExactSciencesColor = GetColor(Math.round(jsonObj.ExactSciences));
        this.ExactSciencesWidth = ((Math.round(jsonObj.ExactSciences - 100))) + '%';

        this.HumanitiesSciences = Math.round(jsonObj.HumanitiesSciences);
        this.HumanitiesSciencesColor = GetColor(Math.round(jsonObj.HumanitiesSciences));
        this.HumanitiesSciencesWidth = ((Math.round(jsonObj.HumanitiesSciences) - 100)) + '%';

        this.NaturalSciences = Math.round(jsonObj.NaturalSciences);
        this.NaturalSciencesColor = GetColor(Math.round(jsonObj.NaturalSciences));
        this.NaturalSciencesWidth = ((Math.round(jsonObj.NaturalSciences - 100))) + '%';

        if (jsonObj.HumanitiesSciences == null || jsonObj.HumanitiesSciences == 0) { self.HumanitiesSciencesWidth = 0 + '%'; }
        if (jsonObj.ExactSciences == null || jsonObj.ExactSciences == 0) { self.ExactSciencesWidth = 0 + '%'; }
        if (jsonObj.NaturalSciences == null || jsonObj.NaturalSciences == 0) { self.NaturalSciencesWidth = 0 + '%'; }
        this.SchoolFull = ko.observable('/school/' + self.Id);
    };
    return model;
})();



