﻿SchoolFullModel = (function () {
    var model = function (uri, id) {3
        var self = this;
        this.isLoaded = ko.observable(false);
        this.htmlData = ko.observable("");
        this.loadPage = function () {
            $.ajax(
                {
                    type: "POST",
                    url: uri,
                    dataType: "json",
                    traditional: true,
                    data: { Id: id },
                    success: function (data) {
                        self.htmlData(new SchoolFullViewModel(data));               
                        $(window).resize();
                        self.isLoaded(true);
                    }
                });
        }

        this.loadPage();
        

    };

    return model;
})();
