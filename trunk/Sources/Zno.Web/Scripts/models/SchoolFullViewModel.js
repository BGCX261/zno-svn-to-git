﻿SchoolFullViewModel = function () {
    var SubjectsClean = {
        'Math': 'Математика',
        'Phisics': 'Фізика',
        'Biology': 'Біологія',
        'Geography': 'Географія',
        'Chemistry': 'Хімія',
        'HistoryUkr': 'Історія України',
        'HistoryForeign': 'Всесвітня історія',
        'LiteratureForeign': 'Зарубіжна література',
        'Economics': 'Основи економіки',
        'Jurisprudence': 'Основи правознавства',
        'Ukrainian': 'Українська мова',
        'Spanish': 'Іспанська мова',
        'English': 'Англійська мова',
        'Franch': 'Французька мова',
        'Germany': 'Німецька мова',
        'Russian': 'Російська мова'
    }

    var GetRecordByKeys = function (array, key1, value1, key2, value2) {
        var pos = -1;
        $.each(array, function (index, value) {
            if (value[key1] == value1 && value[key2] == value2) {
                pos = index;
                return false;
            }
        });
        return pos;
    }
    // set view data for table
    var createTableData = function (array, pos, value, year) {
        if (pos != -1)
            return { year: year, count: array[pos].Count, rate: value, base: false, countChange: '', rateChange: '' }
        return { year: year, count: '', rate: '', base: false, countChange: '', rateChange: '' }
    }
    // set value color
    var GetColor = function (value) {
        var a = 100, b = 150, c = 170;
        if (value == 0)
            return "red";
        if (value > a && value < b)
            return "red";
        if (value >= b && value < c)
            return "yellow";
        return "green";
    }
    // set hegiht in % by value 
    var GetHeight = function (value) {
        return (value - 100) + '%';
    }
    var AddTableDataExtraInfo = function (a) {
        for (var i = 0; i < a.length; ++i) {
            subject = a[i].tableData;
            var j = 0;
            for (; j < subject.length; ++j)
                if (subject[j].count != "")
                    break;
            if (j == subject.length)
                return;
            subject[j]['base'] = true;

            for (j, c = j + 1; c < subject.length; ++c) {
                if (subject[c].rate != '') {
                    var rC = subject[j].rate - subject[c].rate;
                    var cC = subject[j].count - subject[c].count;
                    subject[c]['rateChange'] = rC / 2;
                    subject[c]['countChange'] = ((100 / subject[j].count) * cC)^0;
                    j = c;
                };
            }
        }
    }

    var createYearInfo = function (years) {
        var result = {};
        result['model'] = [];
        result['generalRatings'] = [];
        // set generalratings
        $.each(years.Info, function (index, value) {
            if (value.Year != 0) {
                var y = value.Year;
                var v = value.GenRating ^ 0 || '';
                var c = GetColor(v);
                var h = GetHeight(v);
                result.generalRatings.push({ year: y, value: v, color: c, height: h });
            }
        });
        // set subject ratings;
        $.each(SubjectsClean, function (subject, displayName) {
            var info = {};
            info.years = [];
            info.tableData = [];
            info.displayName = displayName;
            $.each(years.Info, function (index, value) {
                if (value.Year != 0) {
                    if (value[subject] != null) {
                        var y = value.Year;
                        var v = value[subject] ^ 0 || '';
                        var c = GetColor(v);
                        var h = GetHeight(v);
                        info.years.push({ year: y, value: v, color: c, height: h });
                    } else
                        info.years.push({ year: value.Year, value: '', color: '', height: '' });
                    var i = GetRecordByKeys(years.ZnoInfo, 'SubjectName', displayName, 'Year', value.Year);

                    info.tableData.push(createTableData(years.ZnoInfo, i, v, value.Year));
                }

            });
            //Validate data. If all subject's are null
            if (info.years.length != 0) {
                b = 0;
                for (i = 0; i < info.years.length; ++i)
                    if (info.years[i].value == 0)
                        b++;
                if (b != info.years.length)
                    result.model.push(info);
            }
        });

        AddTableDataExtraInfo(result['model']);

        return result;
    }
    var model = function (jsonObj) {
        self = this;
        this.SchoolInfo = jsonObj.SchoolInfo;
        this.statistics = createYearInfo(jsonObj);
    }
    return model;
}();



//var Subjects =
//    {
//        exactRatings: {
//            'Math': 'Математика',
//            'Phisics': 'Фізика'
//        },
//        natRatings: {
//            'Biology': 'Біологія',
//            'Geography': 'Географія',
//            'Chemistry': 'Хімія'
//        },
//        humRatings: {
//            'HistoryUkr': 'Історія України',
//            'HistoryForeign': 'Всесвітня історія',
//            'LiteratureForeign': 'Зарубіжна література',
//            'Economics': 'Основи економіки',
//            'Jurisprudence': 'Основи правознавства',
//            'Ukrainian': 'Українська мова',
//            'Spanish': 'Іспанська мова',
//            'English': 'Англійська мова',
//            'Germany': 'Французька мова',
//            'Franch': 'Німецька мова',
//            'Russian': 'Російська мова'
//        }
//    }