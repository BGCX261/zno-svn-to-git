﻿$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn(1000);
        } else {
            $('#scroller').fadeOut(1000);
        }
        return true;
    });
    $('#scroller').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 2000);
    });
});