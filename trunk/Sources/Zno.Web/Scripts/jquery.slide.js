var toHide;
var wrapper;
var content;
var wrapperContent;
var durattion = 500;

var wrapperParams = {};
var contentParams = {};

function animateWrapper(param)
{
    toHide ?  0 : wrapperContent.show();
    wrapper.animate({width: param}, durattion, function()
    {
        toHide ?  wrapperContent.hide() : 0;
        toHide = !toHide;
    });
}
function animateContent(param)
{
    content.animate({marginLeft : param}, durattion);
}
function switchMenu(e)
{
    if (toHide)
    {
        animateWrapper(wrapperParams.Min);
        animateContent(contentParams.Min);

    } else
    {
        animateWrapper(wrapperParams.Max);
        animateContent(contentParams.Max);
    }
}

function RunSwitcher()
{
    switcher = wrapper.find('#switcher');
    content = $('.content');
    wrapperContent = wrapper.find('.left-sidebarcontent');
    toHide = true;

    wrapperParams.Max = wrapper.css('width');
    wrapperParams.Min = '0';
    contentParams.Max = content.css('marginLeft');
    contentParams.Min = '50px';

    switcher.click(switchMenu);
}

$(window).ready(function () {
    wrapper = $('.left-sidebar');
    console.log(document.body.scrollTop);
     RunSwitcher();
});

