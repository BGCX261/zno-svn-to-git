﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertDB
{
    public class varPoint
    {
        public varPoint(string s, double p, int pass)
        {
            this.Speciality = s;
            this.Point = p;
            this.PassedCount = pass;
        }
        public varPoint() { }
        public string Speciality { get; set; }
        public double Point { get; set; }
        public int PassedCount { get; set; }
    };
    class RatingZno_Command
    {
        public static void ReplaseKav()
        {
            using (var db = new Rating_SchoolsEntities())
            {
                Zno[] arr = (from z in db.Zno where z.ZnoSchoolName.Contains("“") select z).ToArray();
                for (int i = 0; i < arr.Length; i++ )
                {
                    arr[i].ZnoSchoolName = arr[i].ZnoSchoolName.Replace("“", "’");
                    db.SaveChanges();
                }
            }
        }
       /* public static void ORDERSCH()
        {
             using (var db = new Rating_SchoolsEntities())
             {
                 int k = 0;
                 School[] arr = (from s in db.School orderby s.GeneralRating descending select s ).ToArray();
                 foreach (School sch in arr)
                 {
                     k++;
                     
                     School2 var = new School2
                     {
                         Address = sch.Address,
                         City = sch.City,
                         Description = sch.Description,
                         District = sch.District,
                         DistrictID = sch.DistrictID,
                         ExactSciences = sch.ExactSciences,
                         GeneralRating = sch.GeneralRating,
                         HumanitiesSciences = sch.HumanitiesSciences,
                         Latitude = sch.Latitude,
                         Longitude = sch.Longitude,
                         NaturalSciences = sch.NaturalSciences,
                         SchoolName = sch.SchoolName,
                         SearchString = sch.SearchString
                     };
                     db.School2.Add(var);
                     db.SaveChanges();
                 }
                 Console.WriteLine(k.ToString());
                 Console.ReadKey();
             }
        }*/
        /*public static void EQUALS_SCHOOLS()
        {
            try
            {
                using (var db = new Rating_SchoolsEntities())
                {
                    IQueryable<School> q = (from s1 in db.SchoolSearchString from s2 in db.School where s1.SchoolName.Contains(s2.SchoolName) select s2);
                    //int count = (from s in db.School where (from sss in db.SchoolSearchString select sss.SchoolName).Any(arg => arg.Contains(s.SchoolName)) select s).Count();
                    Console.WriteLine(q.Count().ToString());
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }*/
       /* public static void SerchStr()
        {
            try
            {
                IList<InsertDB.FileParser.SchoolItem> items = FileParser.Parser.GetData("schools.csv");
                using (var db = new Rating_SchoolsEntities())
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        SchoolSearchString newsch = new SchoolSearchString { SchoolName = items[i].SchoolName, Word1 = items[i].Word1, Word2 = items[i].Word2, Word3 = items[i].Word3 };
                        db.SchoolSearchString.Add(newsch);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) { Console.WriteLine(e.Message); Console.ReadKey(); }
        }*/
        public static void SearchSchool()
        {
            using (var db = new Rating_SchoolsEntities())
            {
                int count = 0;
                int cs = 0;
                School[] arrSch = (from s in db.School select s ).ToArray();
                foreach (School sch in arrSch)
                {
                    cs++;
                    string Search = sch.SchoolName;
                    int District = sch.DistrictID;
                    varPoint[] arrPoints = (from z in db.Zno join r in db.Rating on z.ZnoID equals r.ZnoID select 
                                         new { Name = z.ZnoSchoolName, 
                                             DistrictID = z.DistrictID, 
                                             PassedCount = z.PassedCount, 
                                             Speciality = z.Subject.Speciality, 
                                             Point = r.Point }).
                                             Where(arg => (arg.Name == sch.SchoolName && arg.DistrictID == sch.DistrictID)).
                                                 Select(arg => new varPoint{ Speciality = arg.Speciality,
                                                     Point = arg.Point,
                                                     PassedCount = arg.PassedCount}).ToArray();
                    try
                    {
                        if (arrPoints == null || arrPoints.Count() == 0) 
                        {
                            Console.WriteLine("No sch");
                            Console.ReadKey();
                        }
                        int n = arrPoints.Count();
                        if (n > 48) { Console.WriteLine("To match"); Console.ReadKey(); }
                        count+=n;

                        Nullable<double> ex = null;
                        Nullable<double> hum = null;
                        Nullable<double> est = null;
                        Nullable<double> gen = null;
                        double resalt;
                        double ex1 = 0, hum1 = 0, est1 = 0;
                        double k = 0;
                        for (int i = 0; i < 3; i++)
                        {
                            double Sum = 0;
                            resalt = 0;
                            int N = 0;
                            for (int j = 0; j < n; j++)
                            {
                                if (Int32.Parse(arrPoints[j].Speciality) == i)
                                {
                                    Sum += arrPoints[j].Point;
                                    N++;
                                }
                            }
                            if (N != 0) resalt = Sum / (double)N;
                            if (i == 0) { ex = ex1 = Math.Round(resalt, 3); if(ex1!=0)k++; }
                            if (i == 1) { hum = hum1 = Math.Round(resalt, 3); if (hum1 != 0)k++; }
                            if (i == 2) { est = est1 = Math.Round(resalt, 3); if (est1 != 0)k++; }
                        }
                        resalt = 0;
                        if (k != 0) resalt = (ex1 + hum1 + est1) / (double)k;
                        gen = Math.Round(resalt, 3);
                        if (gen == null || gen == 0) { gen = null; }
                        else if (gen < 100 || gen > 200) { Console.WriteLine("Error in gen" + sch.SchoolID.ToString()); }
                        
                        sch.GeneralRating = gen; ;
                        if (ex == null || ex == 0) { ex = null; }
                        else if (ex < 100 || ex > 200) { Console.WriteLine("Error in ex" + sch.SchoolID.ToString()); }
                       
                        sch.ExactSciences = ex;
                        if (hum == null || hum == 0) { hum = null; }
                        else if (hum < 100 || hum > 200) { Console.WriteLine("Error in hum" + sch.SchoolID.ToString()); }
                       
                        sch.HumanitiesSciences = hum;
                        if (est == null || est == 0) { est = null; }
                        else if (est < 100 || est > 200) { Console.WriteLine("Error in est" + sch.SchoolID.ToString()); }
                        
                        sch.NaturalSciences = est;
                        

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }


                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadKey();
                    }
                    
                }
                Console.WriteLine("Count = " + count.ToString());
                Console.ReadKey();
            }
            return;
        }
        public static bool InsertSubject(string Name)
        {
            if (Name.Length > 50 || Name == "") return false;
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    Subject subj = new Subject { SubjectName = Name };
                    db.Subject.Add(subj);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in Insert Subject name: " + Name);
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool DeleteSubject(short ID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    try
                    {
                        IQueryable<Zno> arrZno = (from z in db.Zno where z.SubjectID == ID select z);
                        foreach (Zno zno in arrZno)
                        {
                            db.Zno.Remove(zno);
                            db.SaveChanges();
                        }
                    }
                    catch { };
                    Subject subj = (from s in db.Subject where s.SubjectID == ID select s).Single<Subject>();
                    db.Subject.Remove(subj);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in delete Subject ID: " + ID.ToString());
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool InsertRegion(string Name, int AddID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    if(Name == "" || Name.Length > 255 || Name.Select(arg => arg).Where(arg => Char.IsLetterOrDigit(arg)).Count() == 0) return false;
                    Region region = new Region { RegionName = Name, RegionAddID = AddID };
                    db.Region.Add(region);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in insert Region Name: " + Name +" , AddID: "+AddID);
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool DeleteRegion(short ID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    try
                    {
                        int[] arrDistrict = (from dis in db.District where dis.RegionID == ID select dis.DistrictID).ToArray<int>();
                        foreach (var dis in arrDistrict)
                        {
                            DeleteDistrict(dis);
                        }
                    }
                    catch { };
                    Region region = (from r in db.Region where r.RegionID == ID select r).Single<Region>();
                    db.Region.Remove(region);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in delete Region ID: " + ID.ToString());
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool InsertDistrict(string Name, short RID, string AddID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    if (Name.Length > 255 || Name == "" || Name.Select(arg => arg).Where(arg => Char.IsLetterOrDigit(arg)).Count() == 0) return false;
                    if ((from r in db.Region where r.RegionID == RID select r).Single<Region>().RegionID == RID)
                    {
                        District district;
                        if (AddID == "NULL")
                        {
                            district = new District { DistrictName = Name, RegionID = RID}; db.District.Add(district);
                            db.District.Add(district);
                            db.SaveChanges();
                            return true;
                        }
                        else if (AddID.Select(arg => arg).Where(arg => Char.IsDigit(arg)).Count() == AddID.Length && AddID.Length < 20)
                        {
                            district = new District { DistrictName = Name, RegionID = RID, DistrictAddID = AddID };
                            db.District.Add(district);
                            db.SaveChanges();
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                catch
                {
                    Console.WriteLine("Error in insert District Name: " + Name + ", RegionID: " + RID + ", AddID: " + AddID);
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool DeleteDistrict(int ID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    try
                    {
                        IQueryable<School> arrSchool = (from sc in db.School where sc.DistrictID == ID select sc);
                        foreach (School sch in arrSchool)
                        {
                            db.School.Remove(sch);
                            db.SaveChanges();
                        }
                    }
                    catch { };
                    try
                    {
                        IQueryable<Zno> arrZno = (from zn in db.Zno where zn.DistrictID == ID select zn);
                        foreach (Zno zno in arrZno)
                        {
                            db.Zno.Remove(zno);
                            db.SaveChanges();
                        }
                    }
                    catch { };
                    District district = (from d in db.District where d.DistrictID == ID select d).Single<District>();
                    db.District.Remove(district);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in delete District ID: " + ID.ToString());
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool InserSchool(string Name, int DID, string city, string address, string description, Nullable<double> langitude, Nullable<double> latitude, string Search)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    if (Name.Length > 255 || Name == "" || Name.Select(arg => arg).Where(arg => Char.IsLetterOrDigit(arg)).Count() == 0)
                    {
                        throw new Exception("Error in Name");
                    }
                    try
                    {
                        if ((from d in db.District where d.DistrictID == DID select d).Single<District>().DistrictID == DID)
                        {
                            try
                            {
                                if (address.Length > 255 || address == "" || address.Select(arg => arg).Where(arg => Char.IsLetterOrDigit(arg)).Count() == 0)
                                {
                                    throw new Exception("Not valid Address");
                                }
                                if (city == "NULL") { city = null; }
                                if (description == "NULL") { description = null; }
                                if (Search == "NULL") { Search = null; }
                                if (langitude == 0) { langitude = null; }
                                if (latitude == 0) { latitude = null; }

                                School school = new School { SchoolName = Name, DistrictID = DID, City = city, Address = address, Description = description, Longitude = langitude, Latitude = latitude, SearchString = Search };
                                //Console.WriteLine(school.SchoolName);
                                //Console.WriteLine(school.DistrictID);
                                //string C = "";
                                //if (school.City == null) C = "null";
                                //Console.WriteLine(C);
                                //Console.WriteLine(school.Address);
                                //Console.WriteLine(school.Description);
                                //Console.WriteLine(school.Latitude.ToString());
                                //Console.WriteLine(school.Latitude.ToString());
                                //Console.WriteLine(school.SearchString);
                                //Console.WriteLine("");
                                db.School.Add(school);
                                db.SaveChanges();
                                return true;
                            }
                            catch (Exception e)
                            {
                                throw new Exception(e.Message);
                            }
                        }
                        return false;
                    }
                    catch (Exception e)
                    {
                        if (e.Message == "Not valid Address")
                            throw new Exception(e.Message);
                        else throw new Exception("Not found District");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Error in Insert School Name: " + Name + ", " + e.Message);
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool DeleteSchool(int ID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    School school = (from sc in db.School where sc.SchoolID == ID select sc).Single<School>();
                    db.School.Remove(school);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in delete School ID: " + ID.ToString());
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool InsertZno(short year, string schoolName, int DID, short SubjID, int passedCount, Nullable<int> int1, Nullable<int> int2, Nullable<int> int3, Nullable<int> int4, Nullable<int> int5, Nullable<int> int6, Nullable<int> int7, Nullable<int> int8, Nullable<int> int9, Nullable<int> int10)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    if (year < 2000 || year > 2020) { throw new Exception("Not valid Year"); }
                    if (schoolName.Length > 255 || schoolName == "" || schoolName.Select(arg => arg).Where(arg => Char.IsLetterOrDigit(arg)).Count() == 0)
                    {
                        throw new Exception("Not valid Name");
                    }
                    if (passedCount < 0) throw new Exception("Not valid PassedCount");
                    if (int1 < 0) { int1 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int2 < 0) { int2 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int3 < 0) { int3 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int4 < 0) { int4 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int5 < 0) { int5 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int6 < 0) { int6 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int7 < 0) { int7 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int8 < 0) { int8 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int9 < 0) { int9 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if (int10 < 0) { int10 = null; Console.WriteLine("Interval < 0"); Console.ReadKey(); }
                    if ((from d in db.District where d.DistrictID == DID select d).Single<District>().DistrictID == DID)
                    {
                        try
                        {
                            if ((from s in db.Subject where s.SubjectID == SubjID select s).Single<Subject>().SubjectID == SubjID)
                            {
                                Zno zno = new Zno { Year = year, ZnoSchoolName = schoolName, DistrictID = DID, SubjectID = SubjID, PassedCount = passedCount, Interval1 = int1, Interval2 = int2, Interval3 = int3, Interval4 = int4, Interval5 = int5, Interval6 = int6, Interval7 = int7, Interval8 = int8, Interval9 = int9, Interval10 = int10 };
                                db.Zno.Add(zno);
                                db.SaveChanges();
                                return true;
                            }
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    return false;
                }
                catch(Exception e)
                {
                    Console.WriteLine("Error in insert Zno SchoolName: "+schoolName+", Year: "+year+", Error in: "+ e.Message);
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool DeleteZno(int ID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    Zno zno = (from z in db.Zno where z.ZnoID == ID select z).Single<Zno>();
                    db.Zno.Remove(zno);
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Error in delete Zno ID: " + ID.ToString());
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static bool InsertRating(int ZID)
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    Zno zno = (from z in db.Zno where z.ZnoID == ZID select z).Single<Zno>();
                    Nullable<int>[] interv = new Nullable<int>[10];
                    interv[0] = zno.Interval1;
                    interv[1] = zno.Interval2;
                    interv[2] = zno.Interval3;
                    interv[3] = zno.Interval4;
                    interv[4] = zno.Interval5;
                    interv[5] = zno.Interval6;
                    interv[6] = zno.Interval7;
                    interv[7] = zno.Interval8;
                    interv[8] = zno.Interval9;
                    interv[9] = zno.Interval10;
                    if (interv.Select(arg => arg).Where(arg => arg == null).Count() > 0)
                    {
                        Console.WriteLine("nulls in ZID = " + ZID);
                        return false;
                    }
                    else
                    {
                        Nullable<int> SUM = 0;
                        double[] SrZnach = new double[] { 111.8, 129.8, 143, 156, 167.3, 178, 186.8, 192.8, 197.5, 200 };
                        foreach (Nullable<int> i in interv)
                        {
                            SUM += i;
                        }
                        double[] arr2 = new double[interv.Length];
                        for (int i = 0; i < interv.Length; i++)
                        {
                            arr2[i] = (double)interv[i] / (double)SUM;
                        }
                        double Sum = 0;
                        foreach (double i in arr2)
                            Sum += i;
                        Sum = 0;
                        for (int i = 0; i < arr2.Length; i++)
                        {
                            Sum += arr2[i] * SrZnach[i];
                        }
                        if (Sum < 100 || Sum > 200)
                        {
                            Console.WriteLine("Error in point ID = " + ZID);
                            Console.ReadKey();
                            return false;
                        }
                       /* Console.WriteLine(Math.Round(Sum,3));
                        Console.ReadKey();*/
                        Rating ret = new Rating { ZnoID = ZID, Point = Math.Round(Sum, 3) };
                        db.Rating.Add(ret);
                        db.SaveChanges();
                        return true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    return false;
                }
            }
        }
        public static int GetCountNullInIntervalsZno()
        {
            using (var db = new Rating_SchoolsEntities())
            {
                try
                {
                    return (from z in db.Zno where (z.Interval1 == null || z.Interval2 == null || z.Interval3 == null || z.Interval4 == null || z.Interval5 == null || z.Interval6 == null || z.Interval7 == null || z.Interval8 == null || z.Interval9 == null || z.Interval10 == null) select z).Count<Zno>();
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    return -1;
                }
            }
        }
    }
}
