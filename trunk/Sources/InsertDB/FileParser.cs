﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InsertDB
{
    class FileRegInfo
    {
        public string filename { get; set; }
        public string pattern { get; set; }
        public readonly bool asOne;
        public FileRegInfo(string FName, string RegPatternt, bool asOne = false)
        {
            filename = FName;
            pattern = RegPatternt;
            this.asOne = asOne;
        }
    }
    class item
    {
        public object[] field;
        public item(int size)
        {
            field = new object[size];
        }
    }

    class FileParser
    {
        static protected StreamReader SR;
        static protected int getLines(FileRegInfo file)
        {
            return File.ReadAllLines(file.filename).Length;
        }
        static protected item[] collectionA(FileRegInfo file)
        {
            Open(file);
            int count = getLines(file);
            Console.WriteLine("In File {0} {1} records", file.filename, count);
            item[] Collection = new item[count];
            Regex reg = new Regex(file.pattern);
            for (int i = 0; i < count; ++i)
            {
                string str = SR.ReadLine();
                if (str.Length < 5) continue;
                Match match = reg.Match(str);
                Collection[i] = new item(match.Groups.Count);
                int res = 0;
                for (int j = 0; j < match.Groups.Count; ++j, res = -1)
                {
                    try
                    {
                        if (int.TryParse(match.Groups[j].Value, out res))
                            Collection[i].field[j] = res;
                        else
                            Collection[i].field[j] = match.Groups[j].Value;
                    }
                    catch
                    {
                        Console.WriteLine("Problem in {0} line", i + 1);
                    }
                }
            }
            Close();
            return Collection;
        }
        public class SchoolItem
        {
            public static SchoolItem GetItemFromString(string input, char delim)
            {
                string[] items = input.Split(delim);
                return new SchoolItem()
                {
                    Word1 = items[0],
                    Word2 = items[1],
                    Word3 = items[2],
                    SchoolName = items[3]
                };
            }
            public string Word1 { get; set; }
            public string Word2 { get; set; }
            public string Word3 { get; set; }
            public string SchoolName { get; set; }
        }
        public class Parser
        {
            public static IList<SchoolItem> GetData(string filename, char delim = ';')
            {
                string[] strings = System.IO.File.ReadAllLines(filename);
                IList<SchoolItem> Collection = new List<SchoolItem>();
                for (int i = 0; i < strings.Length; ++i)
                {
                    Collection.Add(SchoolItem.GetItemFromString(strings[i], delim));
                }
                return Collection;
            }
        }
        static protected item[] collectionB(FileRegInfo file)
        {
            Open(file);
            int count = getLines(file);
            Console.WriteLine("In File {0} {1} records", file.filename, count);
            item[] Collection = new item[count];
            Regex reg = new Regex(file.pattern);
            int res;
            for (int i = 0; i < count; ++i)
            {
                MatchCollection matches = reg.Matches(SR.ReadLine());

                Collection[i] = new item(matches.Count);

                for (int j = 0; j < matches.Count; ++j)
                {
                    try
                    {
                        if (int.TryParse(matches[j].Groups[0].Value, out res))
                            Collection[i].field[j] = res;
                        else
                            Collection[i].field[j] = matches[j].Groups[1].Value;
                    }
                    catch
                    {
                        Console.WriteLine("Problem in {0} line", i + 1);
                    }
                }
            }
            Close();
            return Collection;
        }
        static public item[] GetCollection(FileRegInfo file)
        {
            if (file.asOne)
                return collectionA(file);

            return collectionB(file);
        }
        static protected void Open(FileRegInfo file)
        {
            SR = new StreamReader(file.filename);
        }
        static protected void Close()
        {
            SR.Close();
        }
        public static void ToBase()
        {
            FileRegInfo[] files = new FileRegInfo[5];
            files[0] = new FileRegInfo("school_addresses.sql", "([0-9]*), '([^']*)', ([0-9]*), '?([^']*)'?, '([^']*)', '([^']*)', ([NULL0-9.]+), ([NULL0-9.]+), '([^']*)'", true);
            files[1] = new FileRegInfo("iwe_stat_entity.sql", "([0-9NULL]*), ([0-9NULL]*), '([^']+)', ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*), ([0-9NULL]*)", true);
            files[2] = new FileRegInfo("subjects.txt", "\"([\\dа-яА-ЯїіІ ]+)\"");
            files[3] = new FileRegInfo("region.txt", "\"([\\d\\wа-яА-Я'їЄєЇіІ\\-, ]+)\"");
            files[4] = new FileRegInfo("olbast.txt", "\"([\\dа-яА-ЯїіІ\\-. ]+)\"");
            DateTime start = DateTime.Now;
            item[] it = null;
            int parametr = 1;
            int count = 0;
            if (parametr == 0)
            {
                it = FileParser.GetCollection(files[0]);
                List<int> arr = new List<int>();
                for (int i = 0; i < it.Length; i++)
                {
                    if (it[i] == null) continue;
                    string Lang;
                    //try
                    {
                        Lang = it[i].field[7].ToString();
                        if (it[i].field.Length < 9)
                        {
                            Console.WriteLine("Line {0}", i);
                            Console.ReadKey();
                        }
                        count++;
                    }
                    //catch
                    /* {
                         arr.Add(i);
                         continue;
                     }*/
                    string Lat = it[i].field[8].ToString();
                    Nullable<double> langit;
                    Nullable<double> latit;
                    if (Lang == "NULL") langit = null;
                    else
                    {
                        Lang = Lang.Replace('.', ',');
                        langit = Double.Parse(Lang);
                    }
                    if (Lat == "NULL") latit = null;
                    else
                    {
                        Lat = Lat.Replace('.', ',');
                        latit = Double.Parse(Lang);
                    }
                    if (!RatingZno_Command.InserSchool(it[i].field[2].ToString(), Int32.Parse(it[i].field[3].ToString()), it[i].field[4].ToString(), it[i].field[5].ToString(), it[i].field[6].ToString(), langit, latit, it[i].field[9].ToString())) { Console.WriteLine("Stop"); Console.ReadKey(); }
                }
                Console.WriteLine(count);
                Console.ReadKey();
            }
            else if (parametr == 1)
            {
                count = 0;
                it = FileParser.GetCollection(files[1]);
                List<int> arr1 = new List<int>();
                for (int i = 0; i < it.Length; i++)
                {
                    string it1;

                    it1 = it[i].field[8].ToString();

                    string it2 = it[i].field[9].ToString();
                    string it3 = it[i].field[10].ToString();
                    string it4 = it[i].field[11].ToString();
                    string it5 = it[i].field[12].ToString();
                    string it6 = it[i].field[13].ToString();
                    string it7 = it[i].field[14].ToString();
                    string it8 = it[i].field[15].ToString();
                    string it9 = it[i].field[16].ToString();
                    string it10 = it[i].field[17].ToString();
                    Nullable<int> iter1;
                    Nullable<int> iter2;
                    Nullable<int> iter3;
                    Nullable<int> iter4;
                    Nullable<int> iter5;
                    Nullable<int> iter6;
                    Nullable<int> iter7;
                    Nullable<int> iter8;
                    Nullable<int> iter9;
                    Nullable<int> iter10;
                    if (it1 == "NULL") { iter1 = null; }
                    else { iter1 = Int32.Parse(it1); }
                    if (it2 == "NULL") { iter2 = null; }
                    else { iter2 = Int32.Parse(it2); }
                    if (it3 == "NULL") { iter3 = null; }
                    else { iter3 = Int32.Parse(it3); }
                    if (it4 == "NULL") { iter4 = null; }
                    else { iter4 = Int32.Parse(it4); }
                    if (it5 == "NULL") { iter5 = null; }
                    else { iter5 = Int32.Parse(it5); }
                    if (it6 == "NULL") { iter6 = null; }
                    else { iter6 = Int32.Parse(it6); }
                    if (it7 == "NULL") { iter7 = null; }
                    else { iter7 = Int32.Parse(it7); }
                    if (it8 == "NULL") { iter8 = null; }
                    else { iter8 = Int32.Parse(it8); }
                    if (it9 == "NULL") { iter9 = null; }
                    else { iter9 = Int32.Parse(it9); }
                    if (it10 == "NULL") { iter10 = null; }
                    else { iter10 = Int32.Parse(it10); }
                   
                    if (!RatingZno_Command.InsertZno(short.Parse(it[i].field[2].ToString()), it[i].field[3].ToString(), Int32.Parse(it[i].field[4].ToString()), short.Parse(it[i].field[6].ToString()), Int32.Parse(it[i].field[7].ToString()), iter1, iter2, iter3, iter4, iter5, iter6, iter7, iter8, iter9, iter10))
                    {
                        Console.WriteLine("Stop in " + i.ToString());
                        Console.ReadKey();
                    }
                    count++;
                }
                /*Console.WriteLine("End Error: ");
                foreach (int var in arr1)
                {
                    Console.Write(var.ToString() + ", ");
                }*/
                Console.WriteLine("count: " + count);
                Console.ReadKey();//155804
            }
        }
    }
}