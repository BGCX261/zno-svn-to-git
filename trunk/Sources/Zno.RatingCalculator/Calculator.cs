﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using Zno.Business;
using Zno.DomainModels;

namespace Zno.RatingCalculator
{
    public static class Calculator
    {
        public static void DeleteSpace()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var ZnoKapecs = (from z in session.Query<ZnoModel>() select z).ToList();
                    for (int i = 0; i < ZnoKapecs.Count; i++)
                    {
                        if (ZnoKapecs[i].ZnoSchoolName.EndsWith(" ") || ZnoKapecs[i].ZnoSchoolName.StartsWith(" "))
                        {
                            ZnoKapecs[i].ZnoSchoolName = ZnoKapecs[i].ZnoSchoolName.Trim();
                            session.Update(ZnoKapecs[i]);
                        }
                    }
                    transaction.Commit();
                }
            }
        }
        public static void TestZnoKapec()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                var Names = (from s in session.Query<School>() select s.Name).ToList();
                var ZnoKapecNames = (from s in session.Query<ZnoKapec>() select s.ZnoSchoolName).Distinct().ToList();
                var q = Names.Where(n=>ZnoKapecNames.Any(arg=>arg.Contains(n))).ToList();
                var q2 = ZnoKapecNames.Where(n => Names.Any(arg => arg.Contains(n))).ToList();
                var count = (from s in session.Query<ZnoKapec>() where q2.Contains(s.ZnoSchoolName) select s).ToList() ;
                var count2 =
                    (from s in session.QueryOver<ZnoKapec>() where Names.Any(arg => s.ZnoSchoolName.Contains(arg)) select s)
                        .List();
                Console.WriteLine(q.Count+';'+q2.Count+';'+count.Count()+';'+count2.Count);
            }
        }
        public static void GetInvalidZno()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                var districts = (from s in session.Query<School>() select s.District).Distinct().ToList();
                var Names = (from s in session.Query<School>() select s.Name).ToList();
                try
                {
                    var Zno =
                        session.Query<ZnoModel>().Where(z => (districts.Contains(z.District)))
                            .ToList();
                    var Znos = Zno.Where(z => !(Names.Contains(z.ZnoSchoolName))).ToList();
                    int i = 0;
                    using (var transaction = session.BeginTransaction())
                    {
                        foreach (var z in Znos)
                        {
                            i++;
                            var q = new ZnoKapec
                            {
                                ZnoID = z.ZnoID,
                                Subject = z.Subject,
                                PassedCount = z.PassedCount,
                                Rating = z.Rating,
                                Year = z.Year,
                                ZnoSchoolName = z.ZnoSchoolName,
                                District = z.District,
                                Interval1 = z.Interval1,
                                Interval2 = z.Interval2,
                                Interval3 = z.Interval3,
                                Interval4 = z.Interval4,
                                Interval5 = z.Interval5,
                                Interval6 = z.Interval6,
                                Interval7 = z.Interval7,
                                Interval8 = z.Interval8,
                                Interval9 = z.Interval9,
                                Interval10 = z.Interval10
                            };
                            session.Save(q);

                        }
                        transaction.Commit();
                    }
                    Console.WriteLine("ololo");
                    Console.ReadKey();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }


            }
        }

        public static void RatingUkr()
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                using (var trans = ses.BeginTransaction())
                {
                    var Schools = ses.Query<Zno.DomainModels.School>().Select(arg => arg)
                        .OrderByDescending(arg => arg.GeneralRating).ToList<Zno.DomainModels.School>();
                    int count=0;
                    foreach (var s in Schools)
                    {
                        count++;
                        s.RatingUkr = count;
                        ses.Update(s);
                    }
                    trans.Commit();
                }
            }
        }
        public static void RatingDistrict()
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                var Districts = ses.Query<District>().Select(arg => arg).ToList<District>();
                using (var trans = ses.BeginTransaction())
                {
                    foreach( var d in Districts)
                    {
                        var Schools = (from s in ses.Query<School>()
                                           where (s.District.DistrictID == d.DistrictID)
                                           select s 
                                           ).OrderByDescending(arg=>arg.GeneralRating).ToList();
                        int count = 0;
                        foreach (var s in Schools)
                        {
                            count++;
                            s.RatingDistrict = count;
                            ses.Update(s);
                        }
                    }
                    trans.Commit();
                }
            }
        }
        public static void RatingRegion()
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                var Regions = ses.Query<Region>().Select(arg => arg).ToList<Region>();
                using (var trans = ses.BeginTransaction())
                {
                    foreach (var r in Regions)
                    {
                        var Schools = (from s in ses.Query<School>()
                                       where (s.District.Region.RegionID == r.RegionID)
                                       select s
                                           ).OrderByDescending(arg => arg.GeneralRating).ToList();
                        int count = 0;
                        foreach (var s in Schools)
                        {
                            count++;
                            s.RatingRegion = count;
                            ses.Update(s);
                        }
                    }
                    trans.Commit();
                }
            }
        }
        private static double[] AverageValue = { 111.8, 129.8, 143, 156, 167.3, 178, 186.8, 192.8, 197.5, 200 };
        /// <summary>
        /// Normal calculating Zno
        /// </summary>
        public static void CalculateZno()
        {
            Int64 Count = 0;
            var data = RatingMethod.GetAllZnoRecords();
            if (data == null)
            {
                throw new Exception("No data in table. Query return null");
            }

            foreach (ZnoModel item in data)
            {
                Count++;
                double Sum = 0;
                if (item.Interval1 != null)
                    Sum += double.Parse(item.Interval1.ToString());
                if (item.Interval2 != null)
                    Sum += double.Parse(item.Interval2.ToString());
                if (item.Interval3 != null)
                    Sum += double.Parse(item.Interval3.ToString());
                if (item.Interval4 != null)
                    Sum += double.Parse(item.Interval4.ToString());
                if (item.Interval5 != null)
                    Sum += double.Parse(item.Interval5.ToString());
                if (item.Interval6 != null)
                    Sum += double.Parse(item.Interval6.ToString());
                if (item.Interval7 != null)
                    Sum += double.Parse(item.Interval7.ToString());
                if (item.Interval8 != null)
                    Sum += double.Parse(item.Interval8.ToString());
                if (item.Interval9 != null)
                    Sum += double.Parse(item.Interval9.ToString());
                if (item.Interval10 != null)
                    Sum += double.Parse(item.Interval10.ToString());
                double k = 0;
                if (Sum != 0)
                {
                    if (item.Interval1 != null)
                        k += double.Parse(item.Interval1.ToString()) * AverageValue[0] / Sum;
                    if (item.Interval2 != null)
                        k += double.Parse(item.Interval2.ToString()) * AverageValue[1] / Sum;
                    if (item.Interval3 != null)
                        k += double.Parse(item.Interval3.ToString()) * AverageValue[2] / Sum;
                    if (item.Interval4 != null)
                        k += double.Parse(item.Interval4.ToString()) * AverageValue[3] / Sum;
                    if (item.Interval5 != null)
                        k += double.Parse(item.Interval5.ToString()) * AverageValue[4] / Sum;
                    if (item.Interval6 != null)
                        k += double.Parse(item.Interval6.ToString()) * AverageValue[5] / Sum;
                    if (item.Interval7 != null)
                        k += double.Parse(item.Interval7.ToString()) * AverageValue[6] / Sum;
                    if (item.Interval8 != null)
                        k += double.Parse(item.Interval8.ToString()) * AverageValue[7] / Sum;
                    if (item.Interval9 != null)
                        k += double.Parse(item.Interval9.ToString()) * AverageValue[8] / Sum;
                    if (item.Interval10 != null)
                        k += double.Parse(item.Interval10.ToString()) * AverageValue[9] / Sum;
                }
                item.Rating = Math.Round(k, 3);
                if (item.Rating == 0)
                    item.Rating = null;
                else if (item.Rating < 100 || item.Rating > 200) { Console.WriteLine("Error in gen" + item.ZnoSchoolName.ToString()); Console.ReadKey(); }
            }
            using (var ses = NHibernateHelper.OpenSession())
            {
                using (var trans = ses.BeginTransaction())
                {
                    foreach (var item in data)
                    {
                        if (item.Rating != null)
                            ses.Update(item);
                    }
                    trans.Commit();
                }
            }
        }
        /// <summary>
        /// Normal calculating School
        /// </summary>
        public static void CalculateSchool()
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                using (var trans = ses.BeginTransaction())
                {
                    var Schools = ses.Query<Zno.DomainModels.School>().Select(arg => arg).ToList<Zno.DomainModels.School>();
                    foreach (var sch in Schools)
                    {
                        Nullable<double> ex = null;
                        Nullable<double> hum = null;
                        Nullable<double> est = null;
                        Nullable<double> gen = null;
                        double resalt;
                        double ex1 = 0, hum1 = 0, est1 = 0;
                        double k = 0;
                        var Znos = (from z in ses.Query<Zno.DomainModels.ZnoModel>()
                                    where z.ZnoSchoolName == sch.Name
                                    select new { Rating = z.Rating, Speciality = z.Subject.Speciality }).ToList();
                        for (int i = 0; i < 3; i++)
                        {
                            double Sum = 0;
                            resalt = 0;
                            int N = 0;
                            foreach (var zo in Znos)
                            {
                                if (zo.Speciality == i && zo.Rating != null)
                                {
                                    Sum += double.Parse(zo.Rating.ToString());
                                    N++;
                                }
                            }
                            if (N != 0) resalt = Sum / (double)N;
                            if (i == 0) { ex = ex1 = Math.Round(resalt, 3); if (ex1 != 0)k++; }
                            if (i == 1) { hum = hum1 = Math.Round(resalt, 3); if (hum1 != 0)k++; }
                            if (i == 2) { est = est1 = Math.Round(resalt, 3); if (est1 != 0)k++; }
                        }
                        resalt = 0;
                        if (k != 0) resalt = (ex1 + hum1 + est1) / (double)k;
                        gen = Math.Round(resalt, 3);
                        if (gen == null || gen == 0) { gen = null; }
                        else if (gen < 100 || gen > 200) { Console.WriteLine("Error in gen" + sch.Id.ToString()); }

                        sch.GeneralRating = gen; ;
                        if (ex == null || ex == 0) { ex = null; }
                        else if (ex < 100 || ex > 200) { Console.WriteLine("Error in ex" + sch.Id.ToString()); }

                        sch.ExactSciences = ex;
                        if (hum == null || hum == 0) { hum = null; }
                        else if (hum < 100 || hum > 200) { Console.WriteLine("Error in hum" + sch.Id.ToString()); }

                        sch.HumanitiesSciences = hum;
                        if (est == null || est == 0) { est = null; }
                        else if (est < 100 || est > 200) { Console.WriteLine("Error in est" + sch.Id.ToString()); }
                        sch.NaturalSciences = est;
                        ses.Update(sch);
                    }
                    trans.Commit();
                }
            }
        }
        public static double? Agr(double?[] array)
        {
            double? agr = 0;
            int count = 0;
            foreach (double? item in array)
            {
                if (item != 0 && item != null && item >= 100 && item <= 200)
                {
                    agr += item;
                    count++;
                }
            }
            double? agrres = null;
            if (count != 0)
            {
                agrres = agr / count;
            }
            return agrres;
        }
        /// <summary>
        /// Calculating all years
        /// </summary>
        public static void SchoolInfoAllYears()
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                var Schools = ses.Query<School>().Select(arg => arg).ToList<School>();
                using (var tranz = ses.BeginTransaction())
                {
                    foreach(var sch in Schools)
                    {
                        var SchoolInfos = (from s in ses.Query<SchoolInfo>()
                                           where (s.SchoolId == sch.Id)
                                           select s).ToList();
                        var Biology1 = new double?[SchoolInfos.Count];
                        var Chemistry1 = new double?[SchoolInfos.Count];
                        var Math1 = new double?[SchoolInfos.Count];
                        var Economics1 = new double?[SchoolInfos.Count];
                        var English1 = new double?[SchoolInfos.Count];
                        var ForeignLiteraturue1 = new double?[SchoolInfos.Count];
                        var Franch1 = new double?[SchoolInfos.Count];
                        var Geography1 = new double?[SchoolInfos.Count];
                        var Germany1 = new double?[SchoolInfos.Count];
                        var HistoryOfUkr1 = new double?[SchoolInfos.Count];
                        var Jurisprudence1 = new double?[SchoolInfos.Count];
                        var Phisics1 = new double?[SchoolInfos.Count];
                        var Russian1 = new double?[SchoolInfos.Count];
                        var Spanish1 = new double?[SchoolInfos.Count];
                        var Ukrainian1 = new double?[SchoolInfos.Count];
                        var WorldHistory1 = new double?[SchoolInfos.Count];
                        int i=0;
                        foreach (var q in SchoolInfos)
                        {
                            Biology1[i] = q.Biology;
                            Math1[i] = q.Math;
                            Chemistry1[i] = q.Chemistry;
                            Economics1[i] = q.Economics;
                            English1[i] = q.English;
                            ForeignLiteraturue1[i] = q.ForeignLiteraturue;
                            Franch1[i] = q.Franch;
                            Geography1[i] = q.Geography;
                            Germany1[i] = q.Germany;
                            HistoryOfUkr1[i] = q.HistoryOfUkr;
                            Jurisprudence1[i] = q.Jurisprudence;
                            Phisics1[i] = q.Phisics;
                            Russian1[i] = q.Russian;
                            Spanish1[i] = q.Spanish;
                            Ukrainian1[i] = q.Ukrainian;
                            WorldHistory1[i] = q.WorldHistory;
                            i++;
                        }
                        var InfoCommit = new SchoolInfo
                        {
                            SchoolId=sch.Id,
                            Year=0,
                            Biology=Agr(Biology1),
                            Chemistry=Agr(Chemistry1),
                            Economics=Agr(Economics1),
                            English=Agr(English1),
                            ExactRating=null,
                            ForeignLiteraturue=Agr(ForeignLiteraturue1),
                            Franch=Agr(Franch1),
                            GenRating=null,
                            Geography=Agr(Geography1),
                            Germany=Agr(Germany1),
                            HistoryOfUkr=Agr(HistoryOfUkr1),
                            HumRating=null,
                            Jurisprudence=Agr(Jurisprudence1),
                            Math = Agr(Math1),
                            NatRating=null,
                            Phisics = Agr(Phisics1),
                            Russian = Agr(Russian1),
                            Spanish = Agr(Spanish1),
                            Ukrainian = Agr(Ukrainian1),
                            WorldHistory = Agr(WorldHistory1)
                        };
                        ses.Save(InfoCommit);
                    }
                    tranz.Commit();
                }
            }
        }
        /// <summary>
        /// Normal calculating School Info.
        /// </summary>
        public static void CalculateSchoolInfo(int YearStart, int YearEnd)
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                var Schools = ses.Query<Zno.DomainModels.School>().Select(arg => arg).ToList<Zno.DomainModels.School>();
                using (var tranz = ses.BeginTransaction())
                {
                    foreach (var sch in Schools)
                    {
                        for (int year = YearStart; year <= YearEnd; year++)
                        {
                            var Znos = (from z in ses.Query<Zno.DomainModels.ZnoModel>()
                                        where (z.ZnoSchoolName == sch.Name && z.Year == year)
                                        select new 
                                        { Rating = z.Rating, Speciality = z.Subject.Speciality, SubjectId = z.Subject.SubjectID }
                                        ).ToList();
                            SchoolInfo NewSchoolInfo = new SchoolInfo
                            {
                                GenRating = null,
                                HumRating = null,
                                ExactRating = null,
                                NatRating = null,
                                Year = year,
                                SchoolId = sch.Id,
                                HistoryOfUkr = null,
                                Biology = null,
                                WorldHistory = null,
                                Geography = null,
                                ForeignLiteraturue = null,
                                Math = null,
                                Economics = null,
                                Jurisprudence = null,
                                Ukrainian = null,
                                Phisics = null,
                                Chemistry = null,
                                Spanish = null,
                                English = null,
                                Germany = null,
                                Franch = null,
                                Russian = null
                            };
                            if (Znos.Count == 0)
                            {
                                ses.Save(NewSchoolInfo);
                            }
                            else
                            {
                                Nullable<double> ex = null;
                                Nullable<double> hum = null;
                                Nullable<double> est = null;
                                Nullable<double> gen = null;
                                double resalt;
                                double ex1 = 0, hum1 = 0, est1 = 0;
                                double k = 0;
                                for (int i = 0; i < 3; i++)
                                {
                                    double Sum = 0;
                                    resalt = 0;
                                    int N = 0;
                                    foreach (var zo in Znos)
                                    {
                                        if (zo.Speciality == i && zo.Rating != null)
                                        {
                                            Sum += double.Parse(zo.Rating.ToString());
                                            N++;
                                        }
                                    }
                                    if (N != 0) resalt = Sum / (double)N;
                                    if (i == 0) { ex = ex1 = Math.Round(resalt, 3); if (ex1 != 0)k++; }
                                    if (i == 1) { hum = hum1 = Math.Round(resalt, 3); if (hum1 != 0)k++; }
                                    if (i == 2) { est = est1 = Math.Round(resalt, 3); if (est1 != 0)k++; }
                                }
                                resalt = 0;
                                if (k != 0) resalt = (ex1 + hum1 + est1) / (double)k;
                                gen = Math.Round(resalt, 3);
                                if (gen == null || gen == 0) { gen = null; }
                                else if (gen < 100 || gen > 200) { Console.WriteLine("Error in gen" + sch.Id.ToString()); }

                                NewSchoolInfo.GenRating = gen;
                                if (ex == null || ex == 0) { ex = null; }
                                else if (ex < 100 || ex > 200) { Console.WriteLine("Error in ex" + sch.Id.ToString()); }

                                NewSchoolInfo.ExactRating = ex;
                                if (hum == null || hum == 0) { hum = null; }
                                else if (hum < 100 || hum > 200) { Console.WriteLine("Error in hum" + sch.Id.ToString()); }

                                NewSchoolInfo.HumRating = hum;
                                if (est == null || est == 0) { est = null; }
                                else if (est < 100 || est > 200) { Console.WriteLine("Error in est" + sch.Id.ToString()); }
                                NewSchoolInfo.NatRating = est;
                                //=============================================================================
                                foreach (var zo in Znos)
                                {
                                    if (zo.SubjectId == 1)
                                        NewSchoolInfo.HistoryOfUkr = zo.Rating;
                                    if (zo.SubjectId == 2)
                                        NewSchoolInfo.Biology = zo.Rating;
                                    if (zo.SubjectId == 3)
                                        NewSchoolInfo.WorldHistory = zo.Rating;
                                    if (zo.SubjectId == 4)
                                        NewSchoolInfo.Geography = zo.Rating;
                                    if (zo.SubjectId == 5)
                                        NewSchoolInfo.ForeignLiteraturue = zo.Rating;
                                    if (zo.SubjectId == 6)
                                        NewSchoolInfo.Math = zo.Rating;
                                    if (zo.SubjectId == 7)
                                        NewSchoolInfo.Economics = zo.Rating;
                                    if (zo.SubjectId == 8)
                                        NewSchoolInfo.Jurisprudence = zo.Rating;
                                    if (zo.SubjectId == 9)
                                        NewSchoolInfo.Ukrainian = zo.Rating;
                                    if (zo.SubjectId == 10)
                                        NewSchoolInfo.Phisics = zo.Rating;
                                    if (zo.SubjectId == 11)
                                        NewSchoolInfo.Chemistry = zo.Rating;
                                    if (zo.SubjectId == 12)
                                        NewSchoolInfo.Spanish = zo.Rating;
                                    if (zo.SubjectId == 13)
                                        NewSchoolInfo.English = zo.Rating;
                                    if (zo.SubjectId == 14)
                                        NewSchoolInfo.Germany = zo.Rating;
                                    if (zo.SubjectId == 15)
                                        NewSchoolInfo.Franch = zo.Rating;
                                    if (zo.SubjectId == 16)
                                        NewSchoolInfo.Russian = zo.Rating;
                                }
                                //=============================================================================
                                ses.Save(NewSchoolInfo);
                            }
                        }
                    }
                    tranz.Commit();
                }
            }
        }
    }
}
