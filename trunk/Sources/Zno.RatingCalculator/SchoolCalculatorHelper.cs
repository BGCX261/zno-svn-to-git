﻿using NHibernate;
using NHibernate.AdoNet;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zno.Business;
using Zno.DomainModels;

namespace Zno.RatingCalculator
{
    public class SchoolCalculatorHelper
    {
        public static SchoolCalculatorHelper Current
        {
            get
            {
                return new SchoolCalculatorHelper();
            }
        }


        public SchoolCalculatorHelper CalculateZnoInformation(int schoolId)
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                using (var trans = ses.BeginTransaction())
                {
                    var school = ses.QueryOver<School>().Where(x => x.Id == schoolId).SingleOrDefault();
                    var schools = new List<School>() { school };
                    foreach (var sch in schools)
                    {
                        Nullable<double> ex = null;
                        Nullable<double> hum = null;
                        Nullable<double> est = null;
                        Nullable<double> gen = null;
                        double resalt;
                        double ex1 = 0, hum1 = 0, est1 = 0;
                        double k = 0;
                        var Znos = (from z in ses.Query<Zno.DomainModels.ZnoModel>()
                                    where z.ZnoSchoolName == sch.Name
                                    select new { Rating = z.Rating, Speciality = z.Subject.Speciality }).ToList();
                        if (Znos.Count == 0)
                            continue;
                        for (int i = 0; i < 3; i++)
                        {
                            double Sum = 0;
                            resalt = 0;
                            int N = 0;
                            foreach (var zo in Znos)
                            {
                                if (zo.Speciality == i && zo.Rating != null)
                                {
                                    Sum += double.Parse(zo.Rating.ToString());
                                    N++;
                                }
                            }
                            if (N != 0) resalt = Sum / (double)N;
                            if (i == 0) { ex = ex1 = Math.Round(resalt, 3); if (ex1 != 0)k++; }
                            if (i == 1) { hum = hum1 = Math.Round(resalt, 3); if (hum1 != 0)k++; }
                            if (i == 2) { est = est1 = Math.Round(resalt, 3); if (est1 != 0)k++; }
                        }
                        resalt = 0;
                        if (k != 0) resalt = (ex1 + hum1 + est1) / (double)k;
                        gen = Math.Round(resalt, 3);
                        if (gen == null || gen == 0) { gen = null; }
                        else if (gen < 100 || gen > 200) { Console.WriteLine("Error in gen" + sch.Id.ToString()); }

                        sch.GeneralRating = gen; ;
                        if (ex == null || ex == 0) { ex = null; }
                        else if (ex < 100 || ex > 200) { Console.WriteLine("Error in ex" + sch.Id.ToString()); }

                        sch.ExactSciences = ex;
                        if (hum == null || hum == 0) { hum = null; }
                        else if (hum < 100 || hum > 200) { Console.WriteLine("Error in hum" + sch.Id.ToString()); }

                        sch.HumanitiesSciences = hum;
                        if (est == null || est == 0) { est = null; }
                        else if (est < 100 || est > 200) { Console.WriteLine("Error in est" + sch.Id.ToString()); }
                        sch.NaturalSciences = est;
                        ses.Update(sch);
                    }
                    trans.Commit();
                }
            }
            return this;
        }

        public void CalculateSchoolInfo(int YearStart, int YearEnd, int schoolId)
        {
            using (ISession ses = NHibernateHelper.OpenSession())
            {
                var Schools = ses.Query<Zno.DomainModels.School>().Where(x => x.Id == schoolId).ToList<Zno.DomainModels.School>();
                using (var tranz = ses.BeginTransaction())
                {
                    foreach (var sch in Schools)
                    {
                        for (int year = YearStart; year <= YearEnd; year++)
                        {
                            var Znos = (from z in ses.Query<Zno.DomainModels.ZnoModel>()
                                        where (z.ZnoSchoolName == sch.Name && z.Year == year)
                                        select new { Rating = z.Rating, Speciality = z.Subject.Speciality, SubjectId = z.Subject.SubjectID }
                                        ).ToList();
                            SchoolInfo NewSchoolInfo = new SchoolInfo
                            {
                                GenRating = null,
                                HumRating = null,
                                ExactRating = null,
                                NatRating = null,
                                Year = year,
                                SchoolId = sch.Id,
                                HistoryOfUkr = null,
                                Biology = null,
                                WorldHistory = null,
                                Geography = null,
                                ForeignLiteraturue = null,
                                Math = null,
                                Economics = null,
                                Jurisprudence = null,
                                Ukrainian = null,
                                Phisics = null,
                                Chemistry = null,
                                Spanish = null,
                                English = null,
                                Germany = null,
                                Franch = null,
                                Russian = null
                            };
                            if (Znos.Count == 0)
                            {
                                ses.Save(NewSchoolInfo);
                            }
                            else
                            {
                                Nullable<double> ex = null;
                                Nullable<double> hum = null;
                                Nullable<double> est = null;
                                Nullable<double> gen = null;
                                double resalt;
                                double ex1 = 0, hum1 = 0, est1 = 0;
                                double k = 0;
                                for (int i = 0; i < 3; i++)
                                {
                                    double Sum = 0;
                                    resalt = 0;
                                    int N = 0;
                                    foreach (var zo in Znos)
                                    {
                                        if (zo.Speciality == i && zo.Rating != null)
                                        {
                                            Sum += double.Parse(zo.Rating.ToString());
                                            N++;
                                        }
                                    }
                                    if (N != 0) resalt = Sum / (double)N;
                                    if (i == 0) { ex = ex1 = Math.Round(resalt, 3); if (ex1 != 0)k++; }
                                    if (i == 1) { hum = hum1 = Math.Round(resalt, 3); if (hum1 != 0)k++; }
                                    if (i == 2) { est = est1 = Math.Round(resalt, 3); if (est1 != 0)k++; }
                                }
                                resalt = 0;
                                if (k != 0) resalt = (ex1 + hum1 + est1) / (double)k;
                                gen = Math.Round(resalt, 3);
                                if (gen == null || gen == 0) { gen = null; }
                                else if (gen < 100 || gen > 200) { Console.WriteLine("Error in gen" + sch.Id.ToString()); }

                                NewSchoolInfo.GenRating = gen;
                                if (ex == null || ex == 0) { ex = null; }
                                else if (ex < 100 || ex > 200) { Console.WriteLine("Error in ex" + sch.Id.ToString()); }

                                NewSchoolInfo.ExactRating = ex;
                                if (hum == null || hum == 0) { hum = null; }
                                else if (hum < 100 || hum > 200) { Console.WriteLine("Error in hum" + sch.Id.ToString()); }

                                NewSchoolInfo.HumRating = hum;
                                if (est == null || est == 0) { est = null; }
                                else if (est < 100 || est > 200) { Console.WriteLine("Error in est" + sch.Id.ToString()); }
                                NewSchoolInfo.NatRating = est;
                                //=============================================================================
                                foreach (var zo in Znos)
                                {
                                    if (zo.SubjectId == 1)
                                        NewSchoolInfo.HistoryOfUkr = zo.Rating;
                                    if (zo.SubjectId == 2)
                                        NewSchoolInfo.Biology = zo.Rating;
                                    if (zo.SubjectId == 3)
                                        NewSchoolInfo.WorldHistory = zo.Rating;
                                    if (zo.SubjectId == 4)
                                        NewSchoolInfo.Geography = zo.Rating;
                                    if (zo.SubjectId == 5)
                                        NewSchoolInfo.ForeignLiteraturue = zo.Rating;
                                    if (zo.SubjectId == 6)
                                        NewSchoolInfo.Math = zo.Rating;
                                    if (zo.SubjectId == 7)
                                        NewSchoolInfo.Economics = zo.Rating;
                                    if (zo.SubjectId == 8)
                                        NewSchoolInfo.Jurisprudence = zo.Rating;
                                    if (zo.SubjectId == 9)
                                        NewSchoolInfo.Ukrainian = zo.Rating;
                                    if (zo.SubjectId == 10)
                                        NewSchoolInfo.Phisics = zo.Rating;
                                    if (zo.SubjectId == 11)
                                        NewSchoolInfo.Chemistry = zo.Rating;
                                    if (zo.SubjectId == 12)
                                        NewSchoolInfo.Spanish = zo.Rating;
                                    if (zo.SubjectId == 13)
                                        NewSchoolInfo.English = zo.Rating;
                                    if (zo.SubjectId == 14)
                                        NewSchoolInfo.Germany = zo.Rating;
                                    if (zo.SubjectId == 15)
                                        NewSchoolInfo.Franch = zo.Rating;
                                    if (zo.SubjectId == 16)
                                        NewSchoolInfo.Russian = zo.Rating;
                                }
                                //=============================================================================
                                ses.Save(NewSchoolInfo);
                            }
                        }
                    }
                    tranz.Commit();
                }
            }
        }

    }
}
