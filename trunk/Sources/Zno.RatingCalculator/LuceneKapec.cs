﻿using System.Runtime.Remoting.Services;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using Zno.Business;
using Zno.DomainModels;

namespace Zno.RatingCalculator
{
    internal class LuceneKapec
    {
        public static void DoSmth()
        {
            Console.WriteLine("Enter School Name");
           // var response = SearchSchoolNameLucene.SearchSchoolbyName(0, 0, "city", "r", 1);
            Directory directory = FSDirectory.Open(new System.IO.DirectoryInfo(Environment.CurrentDirectory + "\\LuceneSchools"));
            var analizer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            IndexReader Indexreader = IndexReader.Open(directory, true);
            Searcher IndexSearch = new IndexSearcher(Indexreader);

            var QueryParser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, "SchoolName", analizer);
            QueryParser.AllowLeadingWildcard = true;
            Query query = QueryParser.Parse("26");



            //Query query = new TermQuery(new Term("DistrictId","222"));

            //     var multi = MultiFieldQueryParser.Parse(Lucene.Net.Util.Version.LUCENE_30, new string[] { "ліцей", "львівсьий", "фізико-математичний" }, new string[] { "Name" }, analizer);

            WildcardQuery queryw = new WildcardQuery(new Term("SchoolName", "*ліськ*"));
            //Console.WriteLine(query.ToString());
            var queryp = new PrefixQuery(new Term("SchoolName", "М.ЖИТОМИР"));
            var querypaddr = new PrefixQuery(new Term("Address", "миру"));
            //var queryw = new WildcardQuery(new Term("Name","Житомирська школа №26"));


            var queryb = new BooleanQuery();
            queryb.Add(query, Occur.MUST);
            //queryb.Add(multi, Occur.MUST);
            //queryb.Add(queryw, Occur.MUST);
            //queryb.Add(queryp, Occur.MUST);
            //queryb.Add(querypaddr, Occur.MUST);

            Console.WriteLine("Searching for query " + queryb);
            TopDocs resultDocs = IndexSearch.Search(queryb, Indexreader.MaxDoc);

            var hits = resultDocs.ScoreDocs;
            foreach (var hit in hits)
            {
                var DocumentFromSearch = IndexSearch.Doc(hit.Doc);
                Console.WriteLine(DocumentFromSearch.Get("SchoolName"));
            }

        }
        public static void CreateFileSchool(string filename)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                Directory directory = FSDirectory.Open(new System.IO.DirectoryInfo(filename));
                Analyzer analizer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);

                using (var writer = new IndexWriter(directory, analizer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    var response = (from s in session.Query<School>() select s);
                    int i = 0;
                    foreach (var resp in response)
                    {
                        i++;
                        var SchoolItem = new Document();
                        SchoolItem.Add(new Field("Id", resp.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                        SchoolItem.Add(new Field("Name", resp.Name, Field.Store.YES, Field.Index.ANALYZED));
                        //SchoolItem.Add(new Field("Address", resp.Address, Field.Store.YES, Field.Index.ANALYZED));
                        SchoolItem.Add(new Field("DistrictId", resp.District.DistrictID.ToString(), Field.Store.YES,
                            Field.Index.NOT_ANALYZED));
                        //SchoolItem.Add(new Field("District", resp.District.DistrictName, Field.Store.YES, Field.Index.ANALYZED));
                        SchoolItem.Add(new Field("RegionId", resp.District.Region.RegionID.ToString(), Field.Store.YES,
                            Field.Index.NOT_ANALYZED));
                        //SchoolItem.Add(new Field("Region", resp.District.Region.RegionName, Field.Store.YES, Field.Index.ANALYZED));
                        SchoolItem.Add(new Field("City", (resp.City) ?? "", Field.Store.YES, Field.Index.ANALYZED));

                        writer.AddDocument(SchoolItem);

                        writer.Optimize();
                    }
                    Console.WriteLine(i.ToString());
                }
                Console.ReadKey();
            }
        }

        public static void CreateFileInvalidZno(string filename)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                Directory directory = FSDirectory.Open(new System.IO.DirectoryInfo(filename));
                Analyzer analizer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);

                using (var writer = new IndexWriter(directory, analizer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    var response = (from s in session.Query<ZnoKapec>() select s);
                    int i = 0;
                    //12185
                    foreach (var resp in response)
                    {
                        i++;
                        var SchoolItem = new Document();
                        SchoolItem.Add(new Field("ZID", resp.ZapId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                        SchoolItem.Add(new Field("ZnoId", resp.ZnoID.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                        SchoolItem.Add(new Field("ZnoSchoolName", resp.ZnoSchoolName, Field.Store.YES, Field.Index.ANALYZED));
                        //SchoolItem.Add(new Field("Address", resp.Address, Field.Store.YES, Field.Index.ANALYZED));
                        SchoolItem.Add(new Field("DistrictId", resp.District.DistrictID.ToString(), Field.Store.YES,
                            Field.Index.NOT_ANALYZED));
                        //SchoolItem.Add(new Field("District", resp.District.DistrictName, Field.Store.YES, Field.Index.ANALYZED));
                        SchoolItem.Add(new Field("RegionId", resp.District.Region.RegionID.ToString(), Field.Store.YES,
                            Field.Index.NOT_ANALYZED));
                        //SchoolItem.Add(new Field("Region", resp.District.Region.RegionName, Field.Store.YES, Field.Index.ANALYZED));
                       
                        writer.AddDocument(SchoolItem);

                       // writer.Optimize();
                    }
                    writer.Optimize();
                    Console.WriteLine(i.ToString());
                }
                Console.ReadKey();
            }
        }
    }
}

